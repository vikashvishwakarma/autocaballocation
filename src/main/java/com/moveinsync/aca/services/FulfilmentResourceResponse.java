package com.moveinsync.aca.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.moveinsync.aca.models.FulfilmentResourceResponseDayWise;
import com.moveinsync.aca.models.GenericResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class FulfilmentResourceResponse {
  @Autowired
  RestTemplate restTemplate;
  public static Logger logger = LoggerFactory.getLogger(FulfilmentResourceResponse.class);

  public List<FulfilmentResourceResponseDayWise> FulfilmentResourceResImp()
      throws JsonMappingException, JsonProcessingException {
    String finalUrl = "https://devvendor.moveinsync.com/fis/fulfilment/v3.0?fulfilmentResourceType=DRIVER_VEHICLE";
    HttpHeaders headers = new HttpHeaders();
    headers.set("Authorization",
        "eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAH2PS2vDMBCE_4vOQ9CuLPlxSqE-GPqAtvRSB6PaojGN62DZpRDy37t5nHOcb2d3Zg8qLp-qUF0Ie_-92o5xDrswxfXX4Pvdqh0HBeWXeTtO_dyHKNaX54eyea3eyubu_rF6kvkSw9R0YZaNk-FQn0nV1aqwGhdVnu4JqG9E1UrMvm1DjFUXxfxBYBgksHBIkSEHaRCBGGRACciCHCgFZaAcrMGyw2ADTsAW7MApOAPnMBqGYORkAmM3EvYbfrrx0jQXOXjpNL1f4blBvjnKh72fVUFOW7KZ0SlU-NtfQeoEHP8BEuV6DkgBAAA.oR0Y5XsqlUcymOGK271-ZbXhrhmCBVxzvwQyLvf6viY");
    GenericResponse result1 = restTemplate
        .exchange(finalUrl, HttpMethod.GET, new HttpEntity<>(headers), GenericResponse.class).getBody();
    String result = new Gson().toJson(result1.getData());
    ObjectMapper obj = new ObjectMapper();
    obj.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    List<FulfilmentResourceResponseDayWise> frDayWise = new ArrayList<>();
    frDayWise = obj.readValue(result, new TypeReference<List<FulfilmentResourceResponseDayWise>>() {
    });
    System.out.println(frDayWise);
    return frDayWise;

  }
}
