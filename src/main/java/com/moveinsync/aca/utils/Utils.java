package com.moveinsync.aca.utils;

import com.moveinsync.aca.models.Geocode;
import com.moveinsync.aca.models.Route;

import java.util.List;

public class Utils {

  public static double getAerialDist(Geocode pt_1, Geocode pt_2) {
    double lat1 = pt_1.getX();
    double lat2 = pt_2.getX();
    double lng1 = pt_1.getY();
    double lng2 = pt_2.getY();

    double earthRadius = 6371; // miles ( or 6371.0 kilometers)
    double dLat = Math.toRadians(lat2 - lat1);
    double dLng = Math.toRadians(lng2 - lng1);
    double sindLat = Math.sin(dLat / 2);
    double sindLng = Math.sin(dLng / 2);
    double a = Math.pow(sindLat, 2)
        + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
    double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    double dist = earthRadius * c;

    return dist * 1.4;
  }

  public static double getAngle(Geocode pt_1, Geocode pt_2, Geocode pt_3) {
    double angle1 = Math.atan2(pt_2.getY() - pt_1.getY(), pt_2.getX() - pt_1.getX());
    double angle2 = Math.atan2(pt_2.getY() - pt_3.getY(), pt_2.getX() - pt_3.getX());
    double x = angle1 - angle2;
    double angle = 0;

    if (x > 0) {
      angle = x * 360 / (2 * Math.PI);
    } else {
      angle = (2 * Math.PI + x) * 360 / (2 * Math.PI);
    }

    return angle;
  }

  public static void sortRouteByStartTime(List<Route> routes) {

    routes.sort((r1, r2) -> {

      if (r1.getAllocationStartTime().getTime() > r2.getAllocationStartTime().getTime()) {
        return 1;
      }

      if (r1.getAllocationStartTime().getTime() < r2.getAllocationStartTime().getTime()) {
        return -1;
      }

      return 0;
    });

  }

  public static Geocode getMidPoint(Geocode startLoc, Geocode endLoc, double garageDistFromStart) {
    double x_mid = startLoc.getX() + ((endLoc.getX() - startLoc.getX()) / garageDistFromStart);
    double y_mid = startLoc.getY() + ((endLoc.getY() - startLoc.getY()) / garageDistFromStart);

    return new Geocode(String.format("%.6f", x_mid) + "," + String.format("%.6f", y_mid));
  }

}
