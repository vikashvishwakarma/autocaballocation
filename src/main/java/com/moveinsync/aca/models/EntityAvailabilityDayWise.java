package com.moveinsync.aca.models;



import org.json.simple.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data 
public class EntityAvailabilityDayWise implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private List<String> workingDays = new ArrayList<>();
  private String startTime;
  private String endTime;
  private boolean isOnline;
  private long lastOnlineTime;
  private JSONObject dayWiseLocation;
  public org.json.simple.JSONObject getDayWiseLocation() {
    return dayWiseLocation;
  }
  public void setDayWiseLocation(org.json.simple.JSONObject dayWiseLocation) {
    this.dayWiseLocation = dayWiseLocation;
  }
  public List<String> getWorkingDays() {
    return workingDays;
  }
  public void setWorkingDays(List<String> workingDays) {
    this.workingDays = workingDays;
  }
  public String getStartTime() {
    return startTime;
  }
  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }
  public String getEndTime() {
    return endTime;
  }
  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }
  public boolean isOnline() {
    return isOnline;
  }
  public void setOnline(boolean isOnline) {
    this.isOnline = isOnline;
  }
  public long getLastOnlineTime() {
    return lastOnlineTime;
  }
  public void setLastOnlineTime(long lastOnlineTime) {
    this.lastOnlineTime = lastOnlineTime;
  }
  
  

}