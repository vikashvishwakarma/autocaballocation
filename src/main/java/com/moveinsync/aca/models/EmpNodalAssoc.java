package com.moveinsync.aca.models;


public class EmpNodalAssoc {
  

  private NodalPoint nodal;
  private double distToNodal;
  
  public EmpNodalAssoc(NodalPoint nodal, double distToNodal) {
      super();
      this.nodal = nodal;
      this.distToNodal = distToNodal;
  }

  public double getDistToNodal() {
      return distToNodal;
  }
  
  public void setDistToNodal(double distToNodal) {
      this.distToNodal = distToNodal;
  }

  public NodalPoint getNodal() {
      return nodal;
  }

  public void setNodal(NodalPoint nodal) {
      this.nodal = nodal;
  }
  
  public boolean equals(EmpNodalAssoc assoc){
      return this.getNodal().equals(assoc.getNodal());
  }
  
  @Override
  public String toString(){
      return this.nodal + "";
      
  }
  


}
