package com.moveinsync.aca.models;

public class GenericResponse<T> {
    private boolean success;
    private String message;
    private T data;
    public GenericResponse() {
    }
    public boolean isSuccess() {
        return this.success;
    }
    public GenericResponse<T> setSuccess(boolean success) {
        this.success = success;
        return this;
    }
    public String getMessage() {
        return this.message;
    }
    public GenericResponse<T> setMessage(String message) {
        this.message = message;
        return this;
    }
    public T getData() {
        return this.data;
    }
    public GenericResponse<T> setData(T data) {
        this.data = data;
        return this;
    }
}