package com.moveinsync.aca.models;

import java.util.Date;

public class Event implements Comparable<Event> {

  private int eventType;
  private Date eventStart;
  private Date eventEnd;
  private Geocode start;
  private Geocode end;
  private double distance;
  private double chargeRemaining;
  private String eventId = "";
  private int duty = 0;
  private String startLegLoc;
  private String endLegLoc;
  private String comments;

  public Event(int eventType, Date eventStart, Date eventEnd, double distance, Geocode start, Geocode end,
      String startLoc, String endLoc) {
    super();
    this.eventType = eventType;
    this.eventStart = eventStart;
    this.eventEnd = eventEnd;
    this.start = start;
    this.end = end;
    this.distance = distance;
  }

  public int getEventType() {
    return eventType;
  }

  public void setEventType(int eventType) {
    this.eventType = eventType;
  }

  public Date getEventStart() {
    return eventStart;
  }

  public void setEventStart(Date eventStart) {
    this.eventStart = eventStart;
  }

  public Date getEventEnd() {
    return eventEnd;
  }

  public void setEventEnd(Date eventEnd) {
    this.eventEnd = eventEnd;
  }

  public Geocode getStart() {
    return start;
  }

  public void setStart(Geocode start) {
    this.start = start;
  }

  public Geocode getEnd() {
    return end;
  }

  public void setEnd(Geocode end) {
    this.end = end;
  }

  public double getDistance() {
    return distance;
  }

  public void setDistance(double distance) {
    this.distance = distance;
  }

  public double getChargeRemaining() {
    return chargeRemaining;
  }

  public void setChargeRemaining(double chargeRemaining) {
    this.chargeRemaining = chargeRemaining;
  }

  public String getEventId() {
    return eventId;
  }

  public void setEventId(String eventId) {
    this.eventId = eventId;
  }

  public int getDuty() {
    return duty;
  }

  public void setDuty(int duty) {
    this.duty = duty;
  }

  public String getStartLegLoc() {
    return startLegLoc;
  }

  public void setStartLegLoc(String startLegLoc) {
    this.startLegLoc = startLegLoc;
  }

  public String getEndLegLoc() {
    return endLegLoc;
  }

  public void setEndLegLoc(String endLegLoc) {
    this.endLegLoc = endLegLoc;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @Override
  public int compareTo(Event o) {
    if(this.getEventStart().before(o.getEventStart())) {
      return -1;
  }
  
  if(this.getEventStart().after(o.getEventStart())) {
      return 1;
  }
  
  if(this.getEventEnd().before(o.getEventEnd())) {
      return -1;
  }
  
  if(this.getEventEnd().after(o.getEventEnd())) {
      return 1;
  }
  
  return 0;
}

  @Override
  public String toString() {
    return this.getEventId() + "(" + this.getEventType() + ")" + " " + this.getEventStart() + " to " + this.getEventEnd();
    }
  
  

}
