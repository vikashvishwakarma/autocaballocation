package com.moveinsync.aca.models;

public class Geocode {
  

  private double latitude;
  private double longitude;
  
  public Geocode(String loc){
      this.latitude = Double.parseDouble(loc.split(",")[0]);
      this.longitude = Double.parseDouble(loc.split(",")[1]);
  }
  
  public double getY() {
      return longitude;
  }
  
  public void setY(double y) {
      this.longitude = y;
  }

  public double getX() {
      return latitude;
  }

  public void setX(double x) {
      this.latitude = x;
  }
  
  @Override
  public String toString(){
      return this.latitude + "," + this.longitude;
  }


}
