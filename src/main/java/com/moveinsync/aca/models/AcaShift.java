package com.moveinsync.aca.models;

import lombok.Data;

@Data
public class AcaShift {
	private String buid;
	private String shiftId;
	private String shift;
	
	
  public String getBuid() {
    return buid;
  }
  public void setBuid(String buid) {
    this.buid = buid;
  }
  public String getShiftId() {
    return shiftId;
  }
  public void setShiftId(String shiftId) {
    this.shiftId = shiftId;
  }
  public String getShift() {
    return shift;
  }
  public void setShift(String shift) {
    this.shift = shift;
  }
	
}
