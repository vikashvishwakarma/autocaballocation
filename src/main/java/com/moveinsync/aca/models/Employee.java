package com.moveinsync.aca.models;


import java.util.ArrayList;
import java.util.List;

public class Employee {
  

  private String employeeID;
  private double lat;
  private double lon;
  private NodalPoint nodal;
  private String landmark;
  private double distToNodal;
  private List<EmpNodalAssoc> nodalCandidates = null;
  private List<String> localities;
  private List<NodalPoint> nodals = new ArrayList<NodalPoint>();
  
  public String getEmployeeID() {
      return employeeID;
  }
  public void setEmployeeID(String employeeID) {
      this.employeeID = employeeID;
  }
  public double getLat() {
      return lat;
  }
  public void setLat(double lat) {
      this.lat = lat;
  }
  public double getLon() {
      return lon;
  }
  public void setLon(double lon) {
      this.lon = lon;
  }
  public NodalPoint getNodal() {
      return nodal;
  }
  public void setNodal(NodalPoint nodal) {
      this.nodal = nodal;
  }
  
  private double getAerialDistance(double lat1, double lng1, double lat2, double lng2) {
      double earthRadius = 6371; // miles ( or 6371.0 kilometers)
      double dLat = Math.toRadians(lat2 - lat1);
      double dLng = Math.toRadians(lng2 - lng1);
      double sindLat = Math.sin(dLat / 2);
      double sindLng = Math.sin(dLng / 2);
      double a = Math.pow(sindLat, 2)
              + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2));
      double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      double dist = earthRadius * c;

      return dist;
  }

  
  public Employee(String empRow) {
      super();
      System.out.println(empRow);
      if(empRow.contains("@")){
          this.employeeID = empRow.split("@")[0];
          this.landmark = empRow.split("@")[1];
          this.lat = Double.parseDouble(empRow.split("@")[2].split(",")[0]);
          this.lon = Double.parseDouble(empRow.split("@")[2].split(",")[1]);
          this.nodalCandidates = new ArrayList<EmpNodalAssoc>();
      } else{
          this.employeeID = empRow;
      }
      this.localities  = new ArrayList<String>();
  }
  @Override
  public String toString() {
      return employeeID + ";" + lat + ";" + lon + ";" + nodal;
  }
  public double getDistToNodal() {
      return distToNodal;
  }
  public void setDistToNodal(double distToNodal) {
      this.distToNodal = distToNodal;
  }
  public List<EmpNodalAssoc> getNodalCandidates() {
      return nodalCandidates;
  }
  public void setNodalCandidates(List<EmpNodalAssoc> nodalCandidates) {
      this.nodalCandidates = nodalCandidates;
  }
  public void setNodal(EmpNodalAssoc nodalAssoc) {
      this.setNodal(nodalAssoc.getNodal());
      this.setDistToNodal(getAerialDistance(this.lat, this.lon, this.nodal.getLat(), this.nodal.getLon()));
      
      removeEmpFromOtherNodalPoints(nodalAssoc);
      
      this.nodalCandidates = new ArrayList<EmpNodalAssoc>();
      this.nodalCandidates.add(nodalAssoc);
      this.nodal.setNumEmps(this.nodal.getNumEmps() + 1);
      this.nodal.setAvgDist(((this.nodal.getAvgDist()*(this.nodal.getNumEmps()-1))+this.getDistToNodal())/this.nodal.getNumEmps());
  }
  
  private void removeEmpFromOtherNodalPoints(EmpNodalAssoc nodalAssociated) {
      for(EmpNodalAssoc nodalAssoc : this.getNodalCandidates()){
          if(nodalAssociated.equals(nodalAssoc)){
              continue;
          }
          
          if(contains(nodalAssoc.getNodal().getEmployees(),this)){
              nodalAssoc.getNodal().getEmployees().remove(this);
          }
      }   
  }
  private boolean contains(List<Employee> employees, Employee employee) {
      for(Employee emp : employees){
          if(emp.equals(employee)){
              return true;
          }
      }
      return false;
  }
  public String getLandmark() {
      return landmark;
  }
  public void setLandmark(String landmark) {
      this.landmark = landmark;
  }
  
  @Override
  public boolean equals(Object o){
      return this.getEmployeeID().equals(((Employee)o).getEmployeeID());
  }
  public List<String> getLocalities() {
      return localities;
  }
  public void setLocalities(List<String> localities) {
      this.localities = localities;
  }
  public List<NodalPoint> getNodals() {
      return nodals;
  }
  public void setNodals(List<NodalPoint> nodals) {
      this.nodals = nodals;
  }


}
