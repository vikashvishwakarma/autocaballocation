package com.moveinsync.aca.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cab {

  private String cabId;
  private int capacity;
  private String cabType;

  private Date currentDutyStart;
  private Date currentDutyLast;
  private boolean currentDutyOver = false;
  private boolean availableForNextTrip = true;
  private int currentDuty = 0;

  private double distRemaining = 0;
  private double distCharged = 0;
  private int numberOfFastChargesSinceLastFullSlowCharge = 0;
  private boolean lastChargeSlow = false;

  private double distRemaining_re = 0;
  private double distCharged_re = 0;
  private double distDoneInDay = 0;

  private ActualCabPreferences preferences = new ActualCabPreferences();

  private List<Date> chargeStartTimes = new ArrayList<Date>();

  private Map<Integer, List<Route>> dutyWiseAllocatedRoutes = new HashMap<Integer, List<Route>>();
  private Map<Date, Double> swapChargeTimes = new HashMap<Date, Double>();

  private List<Event> events = new ArrayList<Event>();

  private Map<Integer, List<Event>> dutyWiseEvents = new HashMap<Integer, List<Event>>();

  public List<Event> getEvents() {
    return events;
  }

  public void addEmptyLeg(double emptyLegDist, Date startTime, Date endTime, Geocode start, Geocode end,
      String startLoc, String endLoc) {
    if (distRemaining < emptyLegDist) {
      setDistRemaining_re(distRemaining_re - (emptyLegDist - distRemaining));
      setDistRemaining(0);
    } else {
      setDistRemaining(distRemaining - emptyLegDist);
    }

    Event event = new Event(0, startTime, endTime, emptyLegDist, start, end, startLoc, endLoc);
    event.setChargeRemaining(distRemaining + distRemaining_re);
    event.setDuty(this.currentDuty);
    events.add(event);

    addEventWiseDuty(event);

    distDoneInDay = emptyLegDist + distDoneInDay;
  }

  public void addEventWiseDuty(Event event) {
    if (dutyWiseEvents.containsKey(this.currentDuty)) {
      dutyWiseEvents.get(this.currentDuty).add(event);
    } else {
      List<Event> eventsInDuty = new ArrayList<Event>();
      eventsInDuty.add(event);
      dutyWiseEvents.put(this.currentDuty, eventsInDuty);
    }
  }

  public void addRoute(Route r) {
    if (dutyWiseAllocatedRoutes.containsKey(currentDuty)) {
      dutyWiseAllocatedRoutes.get(currentDuty).add(r);
      currentDutyLast = r.getEndTime();

      if (distRemaining < r.getDistance()) {
        setDistRemaining_re(distRemaining_re - (r.getDistance() - distRemaining));
        setDistRemaining(0);
      } else {
        setDistRemaining(distRemaining - r.getDistance());
      }
    } else {
      List<Route> routes = new ArrayList<Route>();
      routes.add(r);
      dutyWiseAllocatedRoutes.put(currentDuty, routes);
      currentDutyStart = r.getStartTime();
      currentDutyLast = r.getEndTime();

      if (distRemaining < r.getDistance()) {
        setDistRemaining_re(r.getDistance() - distRemaining);
        setDistRemaining(0);
      } else {
        setDistRemaining(distRemaining - r.getDistance());
      }
    }

    Event event = new Event(1, r.firstEmp().getPickTime(), r.lastEmp().getDropTime(), r.getDistance(),
        r.firstEmp().getPickLoc(), r.lastEmp().getDropLoc(), r.firstEmp().getPicLandmark(),
        r.lastEmp().getDropLandmark());
    event.setChargeRemaining(distRemaining + distRemaining_re);
    event.setEventId(r.getRouteID());
    event.setDuty(this.currentDuty);
    events.add(event);

    addEventWiseDuty(event);

    distDoneInDay = distDoneInDay + r.getDistance();

  }

  public double totalDistInDuty(int duty) {
    double dist = 0;
    List<Event> eventsInDuty = dutyWiseEvents.get(duty);
    for (int i = 0; i < eventsInDuty.size(); i++) {
      Event event = eventsInDuty.get(i);

      dist = dist + event.getDistance();
    }

    return dist;
  }

  public void addDuty() {
    this.currentDuty++;
  }

  public void addDuty(double fullCharge) {
    this.currentDuty++;
    this.setDistRemaining(fullCharge);
  }

  public int getCurrentDuty() {
    return currentDuty;
  }

  public void setCurrentDuty(int currentDuty) {
    this.currentDuty = currentDuty;
  }

  public Map<Integer, List<Route>> getAllocatedRoutes() {
    return dutyWiseAllocatedRoutes;
  }

  public String getCabId() {
    return cabId;
  }

  public Cab(String cabId, int capacity, String cabType) {
    super();
    this.cabId = cabId;
    this.capacity = capacity;
    this.cabType = cabType;
    this.currentDuty = 0;
  }

  @Override
  public String toString() {
    return cabId + ";" + capacity + ";" + swapChargeTimes.keySet().size();
  }

  public int getCapacity() {
    return capacity;
  }

  public void setCapacity(int capacity) {
    this.capacity = capacity;
  }

  public Date getCurrentDutyStart() {
    return currentDutyStart;
  }

  public void setCurrentDutyStart(Date currentDutyStart) {
    this.currentDutyStart = currentDutyStart;
  }

  public Date getCurrentDutyLast() {
    return currentDutyLast;
  }

  public void setCurrentDutyLast(Date currentDutyLast) {
    this.currentDutyLast = currentDutyLast;
  }

  public double getCurrentDutyHours() {
    return ((double) currentDutyLast.getTime() - (double) currentDutyStart.getTime()) / ((double) 60 * 60 * 1000);
  }

  public boolean isCurrentDutyOver() {
    return currentDutyOver;
  }

  public int getNumberTripsCurrentDuty() {
    return dutyWiseAllocatedRoutes.get(currentDuty).size();
  }

  public void setCurrentDutyOver(boolean currentDutyOver) {
    this.currentDutyOver = currentDutyOver;
  }

  public boolean isAvailableForNextTrip() {
    return availableForNextTrip;
  }

  public void setAvailableForNextTrip(boolean availableForNextTrip) {
    this.availableForNextTrip = availableForNextTrip;
  }

  public String getCabType() {
    return cabType;
  }

  public Map<Date, Double> getSwapChargeTimes() {
    return swapChargeTimes;
  }

  public double getDistCharged() {
    return distCharged;
  }

  public void setDistCharged(double distCharged) {
    this.distCharged = distCharged;
  }

  public double getDistRemaining() {
    return distRemaining;
  }

  public void setDistRemaining(double distRemaining) {
    if (distRemaining < 0) {
      System.out.println(distRemaining);
    }
    this.distRemaining = distRemaining;
  }

  public List<Date> getChargeStartTimes() {
    return chargeStartTimes;
  }

  public void setChargeStartTimes(List<Date> chargeStartTimes) {
    this.chargeStartTimes = chargeStartTimes;
  }

  public int getNumberOfFastChargesSinceLastFullSlowCharge() {
    return numberOfFastChargesSinceLastFullSlowCharge;
  }

  public void setNumberOfFastChargesSinceLastFullSlowCharge(int numberOfFastChargesSinceLastFullSlowCharge) {
    this.numberOfFastChargesSinceLastFullSlowCharge = numberOfFastChargesSinceLastFullSlowCharge;
  }

  public boolean isLastChargeSlow() {
    return lastChargeSlow;
  }

  public void setLastChargeSlow(boolean lastChargeSlow) {
    this.lastChargeSlow = lastChargeSlow;
  }

  public double getDistRemaining_re() {
    return distRemaining_re;
  }

  public void setDistRemaining_re(double distRemaining_re) {
    if (distRemaining_re < 0) {
      System.out.println(distRemaining_re);
    }
    this.distRemaining_re = distRemaining_re;
  }

  public double getDistCharged_re() {
    return distCharged_re;
  }

  public void setDistCharged_re(double distCharged_re) {
    this.distCharged_re = distCharged_re;
  }

  public double getTotalChargeRemaining() {
    return distRemaining + distRemaining_re;
  }

  public Event getFirstTripStart(int duty) {
    List<Event> eventsInDuty = dutyWiseEvents.get(duty);
    for (Event event : eventsInDuty) {
      if (event.getEventType() == 1) {
        return event;
      }
    }

    return null;
  }

  public Event getLastTripEnd(int duty) {
    List<Event> eventsInDuty = dutyWiseEvents.get(duty);
    Event lastEvent = null;
    for (Event event : eventsInDuty) {
      if (event.getEventType() == 1) {
        lastEvent = event;
      }
    }

    return lastEvent;
  }

  public Date swap(Date officeReachTime, double ev_fullCharge, double ev_timeToSwap, Geocode office, String officeLoc) {
    this.getSwapChargeTimes().put(officeReachTime, ev_fullCharge);
    this.setDistCharged(ev_fullCharge);
    this.setDistRemaining(ev_fullCharge);

    double swapEndTime = officeReachTime.getTime() + ev_timeToSwap;
    Date swapEnd = new Date((long) swapEndTime);

    Event event = new Event(2, officeReachTime, swapEnd, 0, office, office, officeLoc, officeLoc);
    this.getEvents().add(event);
    event.setChargeRemaining(this.getDistRemaining() + this.getDistRemaining_re());
    event.setDuty(this.getCurrentDuty());

    addEventWiseDuty(event);

    return swapEnd;
  }

  public Date charge(Date officeReachTime, double distanceChargedTo, double ev_slowchargePerMin, double ev_chargePerMin,
      Geocode office, String officeLoc) {
    this.getSwapChargeTimes().put(officeReachTime, distanceChargedTo - this.getDistRemaining());
    this.setDistCharged(distanceChargedTo - this.getDistRemaining());
    this.setDistRemaining(distanceChargedTo);

    double timeToCharge = 0;
    int eventType;

    if (this.isLastChargeSlow()) {
      timeToCharge = ((this.getDistCharged()) / ev_slowchargePerMin) * (60 * 1000);
      eventType = 3;
    } else {
      timeToCharge = ((this.getDistCharged()) / ev_chargePerMin) * (60 * 1000);
      eventType = 2;
    }

    double chargeEndTime = officeReachTime.getTime() + timeToCharge;
    Date chargeEnd = new Date((long) chargeEndTime);

    Event event = new Event(eventType, officeReachTime, chargeEnd, 0, office, office, officeLoc, officeLoc);
    this.getEvents().add(event);
    event.setChargeRemaining(this.getDistRemaining() + this.getDistRemaining_re());
    event.setDuty(this.getCurrentDuty());

    addEventWiseDuty(event);

    return chargeEnd;
  }

  public Date rangeExtCharge(double charge, Date officeReachTime, double timeForCharge, Geocode office,
      String officeLoc) {
    this.getSwapChargeTimes().put(officeReachTime, charge - this.getDistRemaining());
    this.setDistCharged(charge - this.getDistRemaining());
    this.setDistRemaining(charge);

    double chargeEndTime = officeReachTime.getTime() + timeForCharge;
    Date chargeEnd = new Date((long) chargeEndTime);

    Event event = new Event(3, officeReachTime, chargeEnd, 0, office, office, officeLoc, officeLoc);
    this.getEvents().add(event);
    event.setChargeRemaining(this.getDistRemaining() + this.getDistRemaining_re());
    event.setDuty(this.getCurrentDuty());

    addEventWiseDuty(event);

    return chargeEnd;
  }

  public ActualCabPreferences getPreferences() {
    return preferences;
  }

  public void setPreferences(ActualCabPreferences preferences) {
    this.preferences = preferences;
  }

  public Date getFirstDayStart() {
    return dutyWiseEvents.get(new Integer(1)).get(0).getEventStart();
  }

  public Geocode getGarage() {
    return dutyWiseEvents.get(new Integer(1)).get(0).getStart();
  }

  public double getDutyHours(int duty) {
    return ((double) this.getLastTripEnd(duty).getEventEnd().getTime()
        - (double) this.getFirstTripStart(duty).getEventStart().getTime()) / ((double) 60 * 60 * 1000);
  }

}
