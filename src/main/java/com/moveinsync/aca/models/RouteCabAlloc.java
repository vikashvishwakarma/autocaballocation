package com.moveinsync.aca.models;

public class RouteCabAlloc {
  
  private String direction;
  private String shift;
  private String routeId;
  private String pickLandmark;
  private String dropLandmark;
  private String distance;
  private String cabType;
  private String startTime;
  private String endTime;
  private String cab;
  private String score;
  
  
  public RouteCabAlloc(String direction, String shift, String routeId, String pickLandmark, String dropLandmark,
      String distance, String cabType, String startTime, String endTime, String cab, String score) {
    super();
    this.direction = direction;
    this.shift = shift;
    this.routeId = routeId;
    this.pickLandmark = pickLandmark;
    this.dropLandmark = dropLandmark;
    this.distance = distance;
    this.cabType = cabType;
    this.startTime = startTime;
    this.endTime = endTime;
    this.cab = cab;
    this.score = score;
  }


  public String getDirection() {
    return direction;
  }


  public void setDirection(String direction) {
    this.direction = direction;
  }


  public String getShift() {
    return shift;
  }


  public void setShift(String shift) {
    this.shift = shift;
  }


  public String getRouteId() {
    return routeId;
  }


  public void setRouteId(String routeId) {
    this.routeId = routeId;
  }


  public String getPickLandmark() {
    return pickLandmark;
  }


  public void setPickLandmark(String pickLandmark) {
    this.pickLandmark = pickLandmark;
  }


  public String getDropLandmark() {
    return dropLandmark;
  }


  public void setDropLandmark(String dropLandmark) {
    this.dropLandmark = dropLandmark;
  }


  public String getDistance() {
    return distance;
  }


  public void setDistance(String distance) {
    this.distance = distance;
  }


  public String getCabType() {
    return cabType;
  }


  public void setCabType(String cabType) {
    this.cabType = cabType;
  }


  public String getStartTime() {
    return startTime;
  }


  public void setStartTime(String startTime) {
    this.startTime = startTime;
  }


  public String getEndTime() {
    return endTime;
  }


  public void setEndTime(String endTime) {
    this.endTime = endTime;
  }


  public String getCab() {
    return cab;
  }


  public void setCab(String cab) {
    this.cab = cab;
  }


  public String getScore() {
    return score;
  }


  public void setScore(String score) {
    this.score = score;
  }
  
  
  
  

}
