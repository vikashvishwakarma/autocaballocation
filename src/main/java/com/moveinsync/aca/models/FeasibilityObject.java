package com.moveinsync.aca.models;

import java.util.Date;

public class FeasibilityObject {

  private boolean feasible = false;
  private String reason = "";
  private Date thisRouteEndTime = null;
  private Date prevRouteEndTime = null;

  @Override
  public String toString() {
    return this.feasible + " " + this.reason;
  }

  public boolean isFeasible() {
    return feasible;
  }

  public void setFeasible(boolean feasible) {
    this.feasible = feasible;
  }

  public boolean isUseLastKnownLocation() {
    return useLastKnownLocation;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public void setUseLastKnownLocation(boolean useLastKnownLocation) {
    this.useLastKnownLocation = useLastKnownLocation;
  }

  public double getDistFromPrev() {
    return distFromPrev;
  }

  public void setDistFromPrev(double distFromPrev) {
    this.distFromPrev = distFromPrev;
  }

  public boolean isFirstTrip() {
    return isFirstTrip;
  }

  public void setFirstTrip(boolean isFirstTrip) {
    this.isFirstTrip = isFirstTrip;
  }

  public boolean isLastTrip() {
    return isLastTrip;
  }

  public void setLastTrip(boolean isLastTrip) {
    this.isLastTrip = isLastTrip;
  }

  public double getDistToNext() {
    return distToNext;
  }

  public void setDistToNext(double distToNext) {
    this.distToNext = distToNext;
  }

  public Date getThisRouteEndTime() {
    return thisRouteEndTime;
  }

  public void setThisRouteEndTime(Date thisRouteEndTime) {
    this.thisRouteEndTime = thisRouteEndTime;
  }

  public Date getPrevRouteEndTime() {
    return prevRouteEndTime;
  }

  public void setPrevRouteEndTime(Date prevRouteEndTime) {
    this.prevRouteEndTime = prevRouteEndTime;
  }

  private boolean useLastKnownLocation = false;

  private double distFromPrev = 0;
  private double distToNext = 0;

  private boolean isFirstTrip = false;
  private boolean isLastTrip = false;

}
