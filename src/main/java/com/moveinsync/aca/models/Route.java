package com.moveinsync.aca.models;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Route {

  private String routeID;
  private List<Employee> empList;
  private String office;
  private String date;
  private String direction;
  private String shiftTime;
  private Date shift;
  private double distance;
  private List<String> localities;
  private List<EmpTravelDetails> empTravelDetails = new ArrayList<EmpTravelDetails>();
  private String cabType;
  private boolean marshalRequired;
  private ActualCabPreferences allocatedActualCab;
  private Cab allocatedCab;
  
  private String guard = "N/A";
  private Date startTime = null;
  private Date endTime = null;
  private String linkedRouteID;
  private boolean chargeAfterThisTrip;
  private Geocode startLoc = null;
  private Geocode endLoc = null;
  private String startLandmark = null;
  private String endLandmark = null;
  private Date allocationStartTime;
  
  private double score = 0;
  
  
  public Route() {
      super();
      this.empList = new ArrayList<Employee>();
      this.localities = new ArrayList<String>();
  }
  
  public void setEndLoc(Geocode endLoc) {
      this.endLoc = endLoc;
  }
  
  public void setStartLoc(Geocode startLoc) {
      this.startLoc = startLoc;
  }
  
  public EmpTravelDetails firstEmp(){
      EmpTravelDetails firstEmp = null;
      
      for(EmpTravelDetails emp : empTravelDetails){
          if(firstEmp == null){
              firstEmp = emp;
          } else {
              if(emp.getPickTime().before(firstEmp.getPickTime())){
                  firstEmp = emp;
              }
          }
      }
      
      return firstEmp;
  }
  
  public EmpTravelDetails lastEmp(){
      EmpTravelDetails lastEmp = null;
      
      /*
       * if(this.direction.equals("Login")){ for(EmpTravelDetails emp :
       * empTravelDetails){ if(lastEmp == null){ lastEmp = emp; } else {
       * if(emp.getPickTime().after(lastEmp.getPickTime())){ lastEmp = emp; } } } }
       * else {
       */
          for(EmpTravelDetails emp : empTravelDetails){
              if(lastEmp == null){
                  lastEmp = emp;
              } else {
                  if(emp.getDropTime().after(lastEmp.getDropTime())){
                      lastEmp = emp;
                  }
              }
          }
      //}
      
      return lastEmp;
  }
  
  
  public Geocode getOffice() {
      if(this.direction.equals("Login")){
          return this.lastEmp().getDropLoc();
      } else {
          return this.firstEmp().getPickLoc();
      }
  }

  
  
  public void setOffice(String office) {
      this.office = office;
  }

  public String getDate() {
      return date;
  }

  public void setDate(String date) {
      this.date = date;
  }

  public String getDirection() {
      return direction;
  }

  public void setDirection(String direction) {
      this.direction = direction;
  }

  public String getShiftTime() {
      return shiftTime;
  }

  public void setShiftTime(String shiftTime) {
      this.shiftTime = shiftTime;
  }

  public Route(String routeID, Employee emp){
      this.setRouteID(routeID);
      this.empList = new ArrayList<Employee>();
      this.empList.add(emp);
      this.localities = new ArrayList<String>(); 
      this.localities.addAll(emp.getLocalities());
  }

  public String getRouteID() {
      return routeID;
  }

  public void setRouteID(String routeID) {
      this.routeID = routeID;
  }
  
  public List<Employee> getEmpList() {
      return empList;
  }
  
  @Override
  public boolean equals(Object o){
      return this.routeID.equals(((Route)o).routeID);
  }
  
  @Override
  public String toString(){
      return this.getRouteID() + "::" + getAllocatedCab() + "::" + getActualAllocatedCab() + "{" + this.getStartTime() + " to "  + this.getEndTime() + "}";
  }

  public List<String> getLocalities() {
      return localities;
  }

  public void setLocalities(List<String> localities) {
      this.localities = localities;
  }

  public List<EmpTravelDetails> getEmpTravelDetails() {
      return empTravelDetails;
  }

  public void setEmpTravelDetails(List<EmpTravelDetails> empTravelDetails) {
      this.empTravelDetails = empTravelDetails;
  }

  public String getCabType() {
      return cabType;
  }

  public void setCabType(String cabType) {
      this.cabType = cabType;
  }

  public boolean isMarshalRequired() {
      return marshalRequired;
  }

  public void setMarshalRequired(boolean marshalRequired) {
      this.marshalRequired = marshalRequired;
  }

  public Date getShift() {
      return shift;
  }

  public void setShift(Date shift) {
      this.shift = shift;
  }

  public Cab getAllocatedCab() {
      return allocatedCab;
  }

  public ActualCabPreferences getActualAllocatedCab() {
      return allocatedActualCab;
  }
  
  public void setAllocatedCab(Cab allocatedCab) {
      this.allocatedCab = allocatedCab;
      allocatedCab.addRoute(this);
  }

  public void setActualAllocatedCab(ActualCabPreferences allocatedCab, int eventType) {
      this.allocatedActualCab = allocatedCab;
      allocatedActualCab.addRoute(this, eventType);
  }
  
  public void clearAllocation() {
      this.allocatedActualCab = null;
  }
  
  public double getDistance() {
      return distance;
  }

  public void setDistance(double distance) {
      this.distance = distance;
  }

  public String getGuardName() {
      return guard;
  }

  public void setGuardName(String guard) {
      this.guard = guard;
  }

  public Date getStartTime() {
      /*
       * if(empTravelDetails.isEmpty()){ return startTime; } else { return
       * this.firstEmp().getPickTime(); }
       */
      
      if(startTime != null) {
          return startTime;
      } else {
          return this.firstEmp().getPickTime();
      }
      
  }

  public Geocode getStartLoc() {
      if(startLoc != null) {
          return startLoc;
      } else {
          return this.firstEmp().getPickLoc();
      }
  }
  
  public Geocode getEndLoc() {
      if(endLoc != null) {
          return endLoc;
      } else {
          return this.lastEmp().getDropLoc();
      }
  }
  
  public void setStartTime(Date startTime) {
      this.startTime = startTime;
  }

  public Date getEndTime() {
      if(empTravelDetails.isEmpty()){
          return endTime;
      } else {
          return this.lastEmp().getDropTime();
      }
      
  }

  public void setEndTime(Date endTime) {
      this.endTime = endTime;
  }

  public String getLinkedRouteID() {
      return linkedRouteID;
  }

  public void setLinkedRouteID(String linkedRouteID) {
      this.linkedRouteID = linkedRouteID;
  }

  public boolean isChargeAfterThisTrip() {
      return chargeAfterThisTrip;
  }

  public void setChargeAfterThisTrip(boolean chargeAfterThisTrip) {
      this.chargeAfterThisTrip = chargeAfterThisTrip;
  }

  public String getOfficeLoc() {
      if(this.direction.equals("Login")){
          return this.lastEmp().getDropLandmark();
      } else {
          return this.firstEmp().getPicLandmark();
      }
  }

  public String getStartLandmark() {
      if(startLandmark != null) {
          return startLandmark;
      } else {
          return firstEmp().getPicLandmark();
      }
  }

  public void setStartLandmark(String startLandmark) {
      this.startLandmark = startLandmark;
  }

  public String getEndLandmark() {
      if(endLandmark != null) {
          return endLandmark;
      } else {
          return lastEmp().getDropLandmark();
      }
  }

  public void setEndLandmark(String endLandmark) {
      this.endLandmark = endLandmark;
  }

  public Date getAllocationStartTime() {
      this.setAllocationStartTime();
      return allocationStartTime;
  }

  public void setAllocationStartTime() {
      endTime = this.getEndTime();
      shift.setYear(endTime.getYear());
      shift.setMonth(endTime.getMonth());
      shift.setDate(endTime.getDate());
      
      if(direction.equalsIgnoreCase("Login")) {
          
          allocationStartTime = new Date(shift.getTime() - (2*60 * 60 * 1000));
      } else {
          allocationStartTime = shift;
      }
      
  }

  public double getScore() {
      return score;
  }

  public void setScore(double score) {
      this.score = score;
  }


}
