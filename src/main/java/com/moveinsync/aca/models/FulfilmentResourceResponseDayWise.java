package com.moveinsync.aca.models;

import org.json.simple.JSONObject;

import java.io.Serializable;
import java.util.Set;

import lombok.Data;
@Data
public class FulfilmentResourceResponseDayWise implements Serializable {
	  private static final long serialVersionUID = 1L;
	  private int fulfillmentResourceId;
	  private FulfilmentResourceType fulfilmentResourceType;
	  private String driverDLId;
	  private String vehicleRegNo;
	  private long operationStartDate;
	  private FulfilmentResourceStatus status;
	  private EntityAvailabilityDayWise EntityAvailabilityDayWise;
	  public EntityAvailabilityDayWise getEntityAvailabilityDayWise() {
	    return EntityAvailabilityDayWise;
	  }
	  public void setEntityAvailabilityDayWise(EntityAvailabilityDayWise entityAvailabilityDayWise) {
	    EntityAvailabilityDayWise = entityAvailabilityDayWise;
	  }
	  private PaymentDetailModel paymentDetail;
	  private JSONObject otherDetail;
	  private String driverName;
	  private String garageGeoCord;
	  private String driverId;
	  private int capacity;
	  private String frId;
	  private String endGarageGeoCord;
	  private String vendorBusinessName;
	  private String vendorOwnerId;
	  private String vendorId;
	  private Set<String> driverLicences;
	  private boolean isOnline;
	  private long lastOnlineTime;
	  public org.json.simple.JSONObject getOtherDetail() {
	    return otherDetail;
	  }
	  public void setOtherDetail(org.json.simple.JSONObject jsonObject) {
	    this.otherDetail = jsonObject;
	  }
    public int getFulfillmentResourceId() {
      return fulfillmentResourceId;
    }
    public void setFulfillmentResourceId(int fulfillmentResourceId) {
      this.fulfillmentResourceId = fulfillmentResourceId;
    }
    public FulfilmentResourceType getFulfilmentResourceType() {
      return fulfilmentResourceType;
    }
    public void setFulfilmentResourceType(FulfilmentResourceType fulfilmentResourceType) {
      this.fulfilmentResourceType = fulfilmentResourceType;
    }
    public String getDriverDLId() {
      return driverDLId;
    }
    public void setDriverDLId(String driverDLId) {
      this.driverDLId = driverDLId;
    }
    public String getVehicleRegNo() {
      return vehicleRegNo;
    }
    public void setVehicleRegNo(String vehicleRegNo) {
      this.vehicleRegNo = vehicleRegNo;
    }
    public long getOperationStartDate() {
      return operationStartDate;
    }
    public void setOperationStartDate(long operationStartDate) {
      this.operationStartDate = operationStartDate;
    }
    public FulfilmentResourceStatus getStatus() {
      return status;
    }
    public void setStatus(FulfilmentResourceStatus status) {
      this.status = status;
    }
    public PaymentDetailModel getPaymentDetail() {
      return paymentDetail;
    }
    public void setPaymentDetail(PaymentDetailModel paymentDetail) {
      this.paymentDetail = paymentDetail;
    }
    public String getDriverName() {
      return driverName;
    }
    public void setDriverName(String driverName) {
      this.driverName = driverName;
    }
    public String getGarageGeoCord() {
      return garageGeoCord;
    }
    public void setGarageGeoCord(String garageGeoCord) {
      this.garageGeoCord = garageGeoCord;
    }
    public String getDriverId() {
      return driverId;
    }
    public void setDriverId(String driverId) {
      this.driverId = driverId;
    }
    public int getCapacity() {
      return capacity;
    }
    public void setCapacity(int capacity) {
      this.capacity = capacity;
    }
    public String getFrId() {
      return frId;
    }
    public void setFrId(String frId) {
      this.frId = frId;
    }
    public String getEndGarageGeoCord() {
      return endGarageGeoCord;
    }
    public void setEndGarageGeoCord(String endGarageGeoCord) {
      this.endGarageGeoCord = endGarageGeoCord;
    }
    public String getVendorBusinessName() {
      return vendorBusinessName;
    }
    public void setVendorBusinessName(String vendorBusinessName) {
      this.vendorBusinessName = vendorBusinessName;
    }
    public String getVendorOwnerId() {
      return vendorOwnerId;
    }
    public void setVendorOwnerId(String vendorOwnerId) {
      this.vendorOwnerId = vendorOwnerId;
    }
    public String getVendorId() {
      return vendorId;
    }
    public void setVendorId(String vendorId) {
      this.vendorId = vendorId;
    }
    public Set<String> getDriverLicences() {
      return driverLicences;
    }
    public void setDriverLicences(Set<String> driverLicences) {
      this.driverLicences = driverLicences;
    }
    public boolean isOnline() {
      return isOnline;
    }
    public void setOnline(boolean isOnline) {
      this.isOnline = isOnline;
    }
    public long getLastOnlineTime() {
      return lastOnlineTime;
    }
    public void setLastOnlineTime(long lastOnlineTime) {
      this.lastOnlineTime = lastOnlineTime;
    }
    public static long getSerialversionuid() {
      return serialVersionUID;
    }
	  
	  
	}