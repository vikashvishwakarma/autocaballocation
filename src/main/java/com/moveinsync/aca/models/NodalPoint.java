package com.moveinsync.aca.models;


import java.util.ArrayList;
import java.util.List;

public class NodalPoint {
  

  private String nodalName;
  private double lat;
  private double lon;
  private int numEmps = 0;
  private double avgDist;
  private List<Employee> employees = null;
  private String nodalAreaID ="";
  private List <NodalPoint> positiveAffinityWith = new ArrayList<NodalPoint>();
  
  public String getNodalName() {
      return nodalName;
  }
  
  public void setNodalName(String nodalName) {
      this.nodalName = nodalName;
  }
  
  public double getLat() {
      return lat;
  }
  
  public void setLat(double lat) {
      this.lat = lat;
  }
  
  public double getLon() {
      return lon;
  }
  
  public void setLon(double lon) {
      this.lon = lon;
  }

  public NodalPoint(String nodalRow) {
      super();
      
      if(nodalRow.contains("@")){
          this.nodalName = nodalRow.split("@")[0];
          
          if(nodalRow.contains(">")){
              this.lat = Double.parseDouble(nodalRow.split("@")[1].split(">")[0].split(",")[0]);
              this.lon = Double.parseDouble(nodalRow.split("@")[1].split(">")[0].split(",")[1]);
              this.setNodalAreaID(nodalRow.split("@")[1].split(">")[1]);
          } else {
              this.lat = Double.parseDouble(nodalRow.split("@")[1].split(",")[0]);
              this.lon = Double.parseDouble(nodalRow.split("@")[1].split(",")[1]);
          }
      } else{
          this.nodalName = nodalRow;
          this.setNodalAreaID(nodalRow);
      }
      
      this.numEmps = 0;
      this.avgDist = 0;
      this.employees = new ArrayList<Employee>();
  }

  @Override
  public String toString() {
      return nodalName + ";" + lat + ";" + lon;
  }

  public int getNumEmps() {
      return numEmps;
  }

  public void setNumEmps(int numEmps) {
      this.numEmps = numEmps;
  }

  public List<Employee> getEmployees() {
      return employees;
  }

  public void setEmployees(List<Employee> employees) {
      this.employees = employees;
  }

  public boolean isBetter(NodalPoint finalNodal) {
      if(this.getNumEmps() > finalNodal.getNumEmps()+5){
          return true;
      }
      if(this.getNumEmps() > finalNodal.getNumEmps() - 5 && this.getAvgDist() < finalNodal.getAvgDist()*0.75){
          return true;
      }
      return this.getEmployees().size() > finalNodal.getEmployees().size();
  }

  public double getAvgDist() {
      return avgDist;
  }

  public void setAvgDist(double avgDist) {
      this.avgDist = avgDist;
  }

  public String getNodalAreaID() {
      return nodalAreaID;
  }

  public void setNodalAreaID(String nodalAreaID) {
      this.nodalAreaID = nodalAreaID;
  }
  
  @Override
  public boolean equals(Object nodal){
      //System.out.println(this.nodalName);
      if(this.nodalName == null){
          //System.out.println("null found");
          return false;
      }
      return (this.nodalName.equals(((NodalPoint) nodal).getNodalName()) && this.lat == ((NodalPoint) nodal).getLat() && this.lon == ((NodalPoint) nodal).getLon());
  }

  public List <NodalPoint> getPositiveAffinityWith() {
      return positiveAffinityWith;
  }

  public void setPositiveAffinityWith(List <NodalPoint> positiveAffinityWith) {
      this.positiveAffinityWith = positiveAffinityWith;
  }



}
