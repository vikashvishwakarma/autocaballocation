package com.moveinsync.aca.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LastLocation {

  Map<String, List<String>> cabLocationMap = new HashMap<String, List<String>>();

  public LastLocation(Map<String, List<String>> cabLocationMap) {
    super();
    this.cabLocationMap = cabLocationMap;
  }

  public Map<String, List<String>> getCabLocationMap() {
    return cabLocationMap;
  }

  public void setCabLocationMap(Map<String, List<String>> cabLocationMap) {
    this.cabLocationMap = cabLocationMap;
  }

}
