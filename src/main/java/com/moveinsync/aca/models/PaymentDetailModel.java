package com.moveinsync.aca.models;

import java.io.Serializable;

import lombok.Data;
@Data
public class PaymentDetailModel implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  private String beneficiaryName;
  private String bank;
  private String accountNumber;
  private String panNumber;
  private String nameOnPanCard;
  private String ifsc;

}