package com.moveinsync.aca.models;

public enum FulfilmentResourceStatus {
	  INACTIVE(0), ACTIVE(1), DELETED(2), SUSPENDED(3);
	  private int type;
	  FulfilmentResourceStatus(int status) {
	    this.type = status;
	  }
	  public int getType() {
	    return type;
	  }
	  public static FulfilmentResourceStatus getStatus(int type) {
	    for (FulfilmentResourceStatus fulfilmentResourceStatus : values()) {
	      if (fulfilmentResourceStatus.getType() == type) {
	        return fulfilmentResourceStatus;
	      }
	    }
	    return null;
	  }
	}