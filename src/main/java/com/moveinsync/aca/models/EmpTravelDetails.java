package com.moveinsync.aca.models;

import java.util.Date;

public class EmpTravelDetails {
  

  private Employee emp;
  private Date pickTime;
  private Date dropTime;
  private Geocode pickLoc;
  private Geocode dropLoc;
  private String picLandmark;
  private String dropLandmark;
  
  public Employee getEmp() {
      return emp;
  }
  
  public void setEmp(Employee emp) {
      this.emp = emp;
  }

  public Date getPickTime() {
      return pickTime;
  }

  public void setPickTime(Date pickTime) {
      this.pickTime = pickTime;
  }

  public Date getDropTime() {
      return dropTime;
  }

  public void setDropTime(Date dropTime) {
      this.dropTime = dropTime;
  }

  public Geocode getPickLoc() {
      return pickLoc;
  }

  public void setPickLoc(Geocode pickLoc) {
      this.pickLoc = pickLoc;
  }

  public Geocode getDropLoc() {
      return dropLoc;
  }

  public void setDropLoc(Geocode dropLoc) {
      this.dropLoc = dropLoc;
  }

  public String getPicLandmark() {
      return picLandmark;
  }

  public void setPicLandmark(String picLandmark) {
      this.picLandmark = picLandmark;
  }

  public String getDropLandmark() {
      return dropLandmark;
  }

  public void setDropLandmark(String dropLandmark) {
      this.dropLandmark = dropLandmark;
  }
  
  


}
