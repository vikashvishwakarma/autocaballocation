package com.moveinsync.aca.models;

public enum FulfilmentResourceType {
	  DRIVER_VEHICLE(0), DRIVER_VENDOR(1), VEHICLE_VENDOR(2);
	  FulfilmentResourceType(int type) {
	    this.type = type;
	  }
	  private int type;
	  public int getType() {
	    return type;
	  }
	  public static FulfilmentResourceType getFulfilmentResourceType(int type) {
	    for (FulfilmentResourceType fulfilmentResourceType : values()) {
	      if (fulfilmentResourceType.getType() == type) {
	        return fulfilmentResourceType;
	      }
	    }
	    return null;
	  }
	}