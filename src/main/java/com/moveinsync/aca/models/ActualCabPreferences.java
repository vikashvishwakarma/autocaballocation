package com.moveinsync.aca.models;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ActualCabPreferences {
  

  private String cabRegnNum;
  private List<String> preferredShifts = new ArrayList<String>();
  
  private Date startTime;
  private Geocode startLoc;

  private Date endTime;
  private Geocode endLoc;
  
  private int startTimeThreshold;
  private int endTimeThreshold;
  private int endTimeExtn;
  
  private double maxKmInDay;
  private int maxTripsInDay;
  
  private Date currentDutyStart;
  private Date currentDutyLast;
  private boolean currentDutyOver = false;
  private boolean availableForNextTrip = true;
  private int currentDuty = 0;
  private double distDoneInDay = 0;
  private int cabBusiness = 0;
  
  private Geocode lastKnownLoc;
  private Date lastKnownTimestamp;
  
  private List<Route> routes = new ArrayList<Route>();
  

  private List<Event> events = new ArrayList<Event>();
  
  private Map<Integer, List<Route>> dutyWiseAllocatedRoutes = new HashMap<Integer, List<Route>>();
  private Map<Integer, List<Event>> dutyWiseEvents = new HashMap<Integer, List<Event>>();
  
  public List<Event> getEvents(){
      Collections.sort(events);
      
      return events;
  }

  public void addEmptyLeg(double emptyLegDist, Date startTime, Date endTime, Geocode start, Geocode end, String startLoc, String endLoc, int eventType, int duty){
      
      
      Event event = new Event(eventType, startTime, endTime, emptyLegDist, start, end, startLoc, endLoc);
      if(this.getCabRegnNum().equalsIgnoreCase("KA-05-AF-9852") && duty == -1) {
          System.out.println("KA-05-AF-9852");
      }
      
      if(duty == 0) {
          this.addDuty();
      }
      
      if(duty == -1) {
          System.out.println("whats happening?");
      }
      
      event.setDuty(duty);
      events.add(event);
      
      addEventWiseDuty(event);
      
      distDoneInDay = emptyLegDist + distDoneInDay;
  }
  
  public Date getStartDutyTime(int duty) {
      Date startDutyTime = new Date(this.getStartTime().getTime() - this.getStartTimeThreshold() * 60 * 1000);
      Date firstEventStartTime = this.getFirstEventStart(duty).getEventStart();
      
      startDutyTime.setYear(firstEventStartTime.getYear());
      startDutyTime.setMonth(firstEventStartTime.getMonth());
      startDutyTime.setDate(firstEventStartTime.getDate());
      
      if(startDutyTime.after(firstEventStartTime)) {
          startDutyTime = new Date(startDutyTime.getTime() - (24*60*60*1000));
      }
      
      return startDutyTime;
      
  }
  
  public Date getEndDutyTime(int duty) {
      Date endDutyTime = this.getEndTime();
      Date firstEventStartTime = dutyWiseEvents.get(duty).get(0).getEventStart();
      
      endDutyTime.setYear(firstEventStartTime.getYear());
      endDutyTime.setMonth(firstEventStartTime.getMonth());
      endDutyTime.setDate(firstEventStartTime.getDate());
      
      if(endDutyTime.before(firstEventStartTime)) {
          endDutyTime = new Date(endDutyTime.getTime() + (24*60*60*1000));
      }
      
      return endDutyTime;
      
  }
  

  public void addEventWiseDuty(Event event){
      if(dutyWiseEvents.containsKey(event.getDuty())){
          dutyWiseEvents.get(event.getDuty()).add(event);
      } else {
          List<Event> eventsInDuty = new ArrayList<Event>();
          eventsInDuty.add(event);
          dutyWiseEvents.put(event.getDuty(), eventsInDuty);
      }
  }

  public void addRoute(Route r, int eventType){
//      
//      if(this.cabRegnNum.equals("KA-05-AF-9852")) {
//          System.out.println("check");
//      }
//      
//      int duty = findDutyForRoute(r.getStartTime());
//      
//      if(duty == -1) {
//          this.addDuty();
//          duty = currentDuty;
//      }
//  
//      
//      if(routes.contains(r)) {
//          if(currentDutyStart.after(r.getStartTime())) {
//              currentDutyStart = r.getStartTime();
//          }
//          
//          if(currentDutyLast.before(r.getEndTime())) {
//              currentDutyLast = r.getEndTime();
//          }
//          
//          for(Event event : this.getDutyWiseEvents().get(duty)) {
//              if(event.getEventType() >=1 && event.getEventId().equalsIgnoreCase(r.getRouteID())) {
//                  event.setEventStart(r.getStartTime());
//                  event.setEventEnd(r.getEndTime());
//                  event.setStart(r.getStartLoc());
//                  event.setEnd(r.getEndLoc());
//                  event.setStartLegLoc(r.getStartLandmark());
//                  event.setEndLegLoc(r.getEndLandmark());
//              }
//          }
//          
//      } else {
//          routes.add(r);
//          if(dutyWiseAllocatedRoutes.containsKey(duty)){
//              dutyWiseAllocatedRoutes.get(duty).add(r);
//              currentDutyLast = r.getEndTime();
//              
//          } else {
//              List<Route> routes_2 = new ArrayList<Route>();
//              routes_2.add(r);
//              dutyWiseAllocatedRoutes.put(duty, routes_2);
//              currentDutyStart = r.getStartTime();
//              currentDutyLast = r.getEndTime(); 
//          }
//          
//          Event event = new Event(eventType, r.getStartTime(), r.getEndTime(), r.getDistance(), r.getStartLoc(), r.getEndLoc(), r.firstEmp().getPicLandmark(), r.lastEmp().getDropLandmark());
//          event.setEventId(r.getRouteID());
//          event.setDuty(duty);
//          events.add(event);
//          
//          addEventWiseDuty(event);
//          
//          distDoneInDay = distDoneInDay + r.getDistance();
//      }
//      
  }
  
  public int findDutyForRoute(Date startTime2) {
      for(int i = 0; i <= this.getCurrentDuty(); i++) {
          if(dutyWiseEvents.containsKey(i) && this.getStartDutyTime(i).before(startTime2) && this.getEndDutyTime(i).after(startTime2)) {
              return i;
          }
      }
      return -1;
  }
  
  public Date getCabDutyStartTimeForRoute(Date startTime) {
      Date startDutyTime = this.getStartTime();
      
      startDutyTime.setYear(startTime.getYear());
      startDutyTime.setMonth(startTime.getMonth());
      startDutyTime.setDate(startTime.getDate());
      
      if(startDutyTime.getTime() - (this.getStartTimeThreshold() * 60 * 1000) > startTime.getTime()) {
          startDutyTime = new Date(startDutyTime.getTime() - (24*60*60*1000));
      }
      
      return startDutyTime;
  }
  
  public Date getCabDutyEndTimeForRoute(Date startTime) {
      Date endDutyTime = this.getEndTime();
      
      endDutyTime.setYear(startTime.getYear());
      endDutyTime.setMonth(startTime.getMonth());
      endDutyTime.setDate(startTime.getDate());
      
      if(endDutyTime.before(startTime)) {
          endDutyTime = new Date(endDutyTime.getTime() + (24*60*60*1000));
      }
      
      return endDutyTime;
  }

  public double totalDistInDuty(int duty){
      double dist = 0;
      List<Event> eventsInDuty = dutyWiseEvents.get(duty);
      for(int i=0; i<eventsInDuty.size(); i++){
          Event event = eventsInDuty.get(i);  
          dist = dist + event.getDistance();
      }
      
      return dist;
  }

  public void addDuty() {
      this.currentDuty++;
  }
  
  public int getCurrentDuty() {
      return currentDuty;
  }

  public void setCurrentDuty(int currentDuty) {
      this.currentDuty = currentDuty;
  }

  public String getCabRegnNum() {
      return cabRegnNum;
  }

  public void setCabRegnNum(String cabRegnNum) {
      this.cabRegnNum = cabRegnNum;
  }

  public Map<Integer, List<Route>> getAllocatedRoutes() {
      return dutyWiseAllocatedRoutes;
  }

  
  public Date getStartTime() {
      return startTime;
  }

  public Date getCurrentDutyStart() {
      return currentDutyStart;
  }

  public void setCurrentDutyStart(Date currentDutyStart) {
      this.currentDutyStart = currentDutyStart;
  }

  public Date getCurrentDutyLast() {
      return currentDutyLast;
  }

  public void setCurrentDutyLast(Date currentDutyLast) {
      this.currentDutyLast = currentDutyLast;
  }

  public boolean isCurrentDutyOver() {
      return currentDutyOver;
  }

  public void setCurrentDutyOver(boolean currentDutyOver) {
      this.currentDutyOver = currentDutyOver;
  }

  public boolean isAvailableForNextTrip() {
      return availableForNextTrip;
  }

  public void setAvailableForNextTrip(boolean availableForNextTrip) {
      this.availableForNextTrip = availableForNextTrip;
  }

  public double getDistDoneInDay() {
      return distDoneInDay;
  }

  public void setDistDoneInDay(double distDoneInDay) {
      this.distDoneInDay = distDoneInDay;
  }

  public Map<Integer, List<Route>> getDutyWiseAllocatedRoutes() {
      return dutyWiseAllocatedRoutes;
  }

  public int numTripsInDuty(int duty) {
      int numTrips = 0;
      if(dutyWiseAllocatedRoutes.containsKey(duty)) {
          numTrips = dutyWiseAllocatedRoutes.get(duty).size();
      }
      return numTrips;
  }
  
  public void setDutyWiseAllocatedRoutes(Map<Integer, List<Route>> dutyWiseAllocatedRoutes) {
      this.dutyWiseAllocatedRoutes = dutyWiseAllocatedRoutes;
  }

  public Map<Integer, List<Event>> getDutyWiseEvents() {
      return dutyWiseEvents;
  }

  public void setDutyWiseEvents(Map<Integer, List<Event>> dutyWiseEvents) {
      this.dutyWiseEvents = dutyWiseEvents;
  }

  public void setPreferredShifts(List<String> preferredShifts) {
      this.preferredShifts = preferredShifts;
  }

  public void setEvents(List<Event> events) {
      this.events = events;
  }

  public void setStartTime(Date startTime) {
      this.startTime = startTime;
  }

  public Geocode getStartLoc() {
      return startLoc;
  }

  public void setStartLoc(Geocode startLoc) {
      this.startLoc = startLoc;
  }

  public Date getEndTime() {
      return endTime;
  }

  public void setEndTime(Date endTime) {
      this.endTime = endTime;
  }

  public Geocode getEndLoc() {
      return endLoc;
  }

  public void setEndLoc(Geocode endLoc) {
      this.endLoc = endLoc;
  }

  public int getEndTimeThreshold() {
      return endTimeThreshold;
  }

  public void setEndTimeThreshold(int endTimeThreshold) {
      this.endTimeThreshold = endTimeThreshold;
  }

  public int getEndTimeExtn() {
      return endTimeExtn;
  }

  public void setEndTimeExtn(int endTimeExtn) {
      this.endTimeExtn = endTimeExtn;
  }

  public List<String> getPreferredShifts() {
      return preferredShifts;
  }

  public void addShift(String preferredShift) {
      this.preferredShifts.add(preferredShift);
  }

  public double getMaxKmInDay() {
      return maxKmInDay;
  }

  public void setMaxKmInDay(double maxKmInDay) {
      this.maxKmInDay = maxKmInDay;
  }

  public int getMaxTripsInDay() {
      return maxTripsInDay;
  }

  public void setMaxTripsInDay(int maxTripsInDay) {
      this.maxTripsInDay = maxTripsInDay;
  }

  public int getStartTimeThreshold() {
      return startTimeThreshold;
  }

  public void setStartTimeThreshold(int startTimeThreshold) {
      this.startTimeThreshold = startTimeThreshold;
  }
  
  @Override
  public String toString() {
      return this.getCabRegnNum();
  }
  
  public Event getLastTripEnd(int duty){
      List<Event> eventsInDuty = dutyWiseEvents.get(duty);
      Event lastEvent = null;
      for(Event event : eventsInDuty){
          if(event.getEventType() >= 1 && (lastEvent == null || event.getEventEnd().after(lastEvent.getEventEnd()))){
              lastEvent = event;
          }
      }
      
      return lastEvent;
  }
  
  public Event getFirstTripStart(int duty){
      List<Event> eventsInDuty = dutyWiseEvents.get(duty);
      Event firstEvent = null;
      for(Event event : eventsInDuty){
          if(event.getEventType() >= 1 && (firstEvent == null || event.getEventStart().before(firstEvent.getEventStart()))){
              firstEvent = event;
          }
      }
      
      return firstEvent;
  }

  
  public Event getFirstEventStart(int duty){
      List<Event> eventsInDuty = dutyWiseEvents.get(duty);
      Event firstEvent = null;
      for(Event event : eventsInDuty){
          if((firstEvent == null || event.getEventStart().before(firstEvent.getEventStart()))){
              firstEvent = event;
          }
      }
      
      return firstEvent;
  }

  public double getDutyHours(int duty) {
      if(this.getLastTripEnd(duty) == null || this.getFirstTripStart(duty) == null) {
        return 0d;
      }
      return ((double)this.getLastTripEnd(duty).getEventEnd().getTime() - (double)this.getFirstTripStart(duty).getEventStart().getTime())/((double)60*60*1000);
  }

  public int getCabBusiness() {
      return cabBusiness;
  }

  public void setCabBusiness(int cabBusiness) {
      this.cabBusiness = cabBusiness;
  }

  public Geocode getLastKnownLoc() {
      return lastKnownLoc;
  }

  public void setLastKnownLoc(Geocode lastKnownLoc) {
      this.lastKnownLoc = lastKnownLoc;
  }

  public Date getLastKnownTimestamp() {
      return lastKnownTimestamp;
  }

  public void setLastKnownTimestamp(Date lastKnownTimestamp) {
      this.lastKnownTimestamp = lastKnownTimestamp;
  }

  public void clearAllEvents() {
      this.currentDuty = 0;
      this.distDoneInDay = 0;
      this.routes = new ArrayList<Route>();
      this.dutyWiseAllocatedRoutes = new HashMap<Integer, List<Route>>();
      this.dutyWiseEvents = new HashMap<Integer, List<Event>>();
      this.events = new ArrayList<Event>();
  }   


}
