package com.moveinsync.aca.application;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import com.moveinsync.aca.services.FulfilmentResourceResponse;

@Configuration
public class BeanConfiguration {
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	@Bean
    public FulfilmentResourceResponse getfulfilmentResourceResponse() {
	return new FulfilmentResourceResponse();
	
}
}
