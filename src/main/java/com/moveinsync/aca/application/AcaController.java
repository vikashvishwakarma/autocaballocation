package com.moveinsync.aca.application;

import com.moveinsync.aca.models.AcaShift;
import com.moveinsync.aca.models.RouteCabAlloc;
import com.moveinsync.aca.services.FulfilmentResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cab-allocation/")
@CrossOrigin
public class AcaController extends AcaControllerAutoWired {

	private final Logger LOG =  LoggerFactory.getLogger(AcaController.class);
	@Autowired
	Allocator ac;

	@Autowired
	FulfilmentResourceResponse fulfilmentResourceResponse;

	@ResponseBody
	@RequestMapping(value="allocate",method = RequestMethod.GET)
  public String cabAllocator() throws Exception {
    ac.allocate(null);
    return "Hello World";
  }

//	@CrossOrigin
//	@GetMapping("/cabRegNo/{cab}")
//	public @ResponseBody ResponseEntity<String> getCabDetail(@PathVariable(value = "cab") String cabReg){
//
//		try {
//			//			ac.allocate();
//			List<FulfilmentResourceResponseDayWise> frDayWise = 
//					fulfilmentResourceResponse
//					.FulfilmentResourceResImp()
//					.stream()
//					.filter(x->cabReg.equalsIgnoreCase(x.getVehicleRegNo()))
//					.collect(Collectors.toList());				  																		
//
//			String entities=new Gson().toJson(frDayWise);
//			LOG.info("list of Dettails associated with cab " + new Gson().toJson(frDayWise)); 
//			return ResponseEntity.ok().body(entities);
//		} catch (JsonProcessingException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("INTERNAL ERROR");
//
//		}
//	}
	
	@CrossOrigin
	@RequestMapping(value = "/acatools", method = RequestMethod.POST,consumes = MediaType.ALL_VALUE)
	public List<RouteCabAlloc> acaShiftDetails(@RequestBody AcaShift aca []) throws Exception{
		return ac.allocate(aca);
	}

}
