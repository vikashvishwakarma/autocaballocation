package com.moveinsync.aca.application;



import com.moveinsync.tripsheetservice.client.TripsheetClient.Builder;
import com.moveinsync.tripsheetservice.client.TripsheetServiceClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;



@SpringBootApplication
@ComponentScan
public class AcaApplication {

  public static void main(String[] args) {
    SpringApplication.run(AcaApplication.class, args);
  }
  
  @Bean
  public TripsheetServiceClient getTripsheetClient() {
    Builder builder = new Builder();
    builder.withUrl("http://tripbeta.moveinsync.com:9080/");
    return new TripsheetServiceClient(builder);
  }

}
