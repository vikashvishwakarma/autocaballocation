package com.moveinsync.aca.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.gson.Gson;
import com.moveinsync.aca.models.AcaShift;
import com.moveinsync.aca.models.ActualCabPreferences;
import com.moveinsync.aca.models.EmpTravelDetails;
import com.moveinsync.aca.models.Employee;
import com.moveinsync.aca.models.Event;
import com.moveinsync.aca.models.FeasibilityObject;
import com.moveinsync.aca.models.FulfilmentResourceResponseDayWise;
import com.moveinsync.aca.models.Geocode;
import com.moveinsync.aca.models.LastLocation;
import com.moveinsync.aca.models.Route;
import com.moveinsync.aca.models.RouteCabAlloc;
import com.moveinsync.aca.utils.Utils;
import com.moveinsync.tripsheet.models.EmployeeTripsheetDTOV2;
import com.moveinsync.tripsheet.models.TripsheetDTOV2;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
@Component
public class Allocator extends AllocatorResource{


	public static Logger logger = LoggerFactory.getLogger(Allocator.class);
	static Map<String, Route> routes = new HashMap<String, Route>();
	static Map<String, Map<Date, List<String>>> shiftWiseRoutes = new HashMap<String, Map<Date, List<String>>>();
	static Map<Integer, Double> speedProfile = new HashMap<Integer, Double>();
	static Map<Integer, Map<String, ActualCabPreferences>> actualCabs = new HashMap<Integer, Map<String, ActualCabPreferences>>();
	static Map<String, Route> otherRoutes = new HashMap<String, Route>();
	private static int bufferTime;
	private static boolean simulateLastKnownLoc = false;
	static Map<String, Map<Integer, Double>> costPerKm = new HashMap<String, Map<Integer, Double>>();
	static Map<String, Map<Integer, Double>> costPerTrip = new HashMap<String, Map<Integer, Double>>();
	static Map<Integer, Double> costPerWaitingMin = new HashMap<Integer, Double>();
	static Map<Route, ActualCabPreferences> prevIterAlloc = new HashMap<Route, ActualCabPreferences>();
	static int cabRegnNumStart = 0;

	private static double maxIterationsToRun;
	static long minutes = 60 * 1000;
	static long hours = 60 * minutes;
	private static SimpleDateFormat sysDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM HH-mm");
	private static int batch = 0;
	private static int garageLogin = 0;
	private static int garageLogout = 0;
	private static int cabChanges = 0;

	private static double numIter = 0;
	DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
	Date shiftDate = new Date();
	String today = formatter.format(shiftDate);
	@Autowired
	private RestTemplate restTemplate;

	// @Autowired
	// private Environment env;
	//
	// server.host=9090
	// configuration:
	// Url:
	// loadrouteUrl:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// loadPropertiesUrl:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// loadCabPreferences:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// loadCabDeployment:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// LastKnownLoc:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// getLastKnownLoc:"https://ds.moveinsync.com/ds/location/last-location/registrations"
	// @Value("{$configuration.Url.loadPropertiesUrl}")
	public List<RouteCabAlloc> allocate(AcaShift aca []) throws ParseException {
	  
	  List<RouteCabAlloc> resultList = new ArrayList<RouteCabAlloc>();

		logger.debug("Logs-" + new Date() + ".txt", "UTF-8");
		logger.debug("Cab;Route;Feasible;Score;Reason;Secondary Reason;"
				+ "Last Location Type;Last Location;Route Start;Route End;Next Location;Next Location Type;"
				+ "First Trip;Last Trip;" + "Empty Leg To Start;Empty Leg to Next;" + "Prev Trip End Time;This Trip End Time");

		logger.debug("RunLogs-" + new Date() + ".txt", "UTF-8");
		logger.debug("numIter;Batch;Shift;NumRoutes;Old Cabs;Old Cabs Found Not Used;New Csbs Used");
		
		Map<String, List<String>> buidShiftMap = new HashMap<String, List<String>>();
		
		for (AcaShift acaShift : aca) {
		  if(!buidShiftMap.containsKey(acaShift.getBuid())) {
		    buidShiftMap.put(acaShift.getBuid(), new ArrayList<String>());
		  }
		  buidShiftMap.get(acaShift.getBuid()).add(acaShift.getShift());
        }

		loadProperties();
		loadRoutes(buidShiftMap);
		// createMatrix();

		try {
			loadCabPreferences();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		loadCabDeployment(buidShiftMap);

		if (simulateLastKnownLoc) {
			simulateLastKnownLoc();
		} else {
			getLastKnownLoc();
		}

		simulateCurrentTravelPlan();

		boolean runIter = true;

		while (runIter) {
			allocateCabs();
			if (numIter > maxIterationsToRun) {
				runIter = false;
			} else {
				runIter = checkIfNewCabsAddedForThisRun();
				if (runIter) {
					cleanAllocations();

				}
			}

			numIter++;
			System.out.println("Num Iter: " + numIter + " Login Garage " + garageLogin + " Logout Garage " + garageLogout
					+ " Cab Changes " + cabChanges);
			cabChanges = 0;
		}

		logger.debug("RouteCabAlloc-" + new Date() + ".txt", "UTF-8");

		logger.debug("Direction" + ";" + "Shift" + ";" + "Route ID" + ";" + "Pick Landmark" + ";" + "Drop Landmark" + ";"
				+ "Distance" + ";" + "CabType" + ";" + "Start Time" + ";" + "End Time" + ";" + "Cab" + ";" + "Score");
    for (Route route : routes.values()) {
      ActualCabPreferences cab = route.getActualAllocatedCab();
      RouteCabAlloc routeCabAlloc = new RouteCabAlloc(route.getDirection(), route.getShiftTime(), route.getRouteID(),
          route.firstEmp().getPicLandmark(), route.lastEmp().getDropLandmark(), String.valueOf(route.getDistance()),
          route.getCabType(), sysDateFormat.format(route.firstEmp().getPickTime()),
          sysDateFormat.format(route.lastEmp().getDropTime()), cab.getCabRegnNum(), String.valueOf(route.getScore()));
      
      resultList.add(routeCabAlloc);
      
      logger.debug(route.getDirection() + ";" + route.getShiftTime() + ";" + route.getRouteID() + ";"
          + route.firstEmp().getPicLandmark() + ";" + route.lastEmp().getDropLandmark() + ";" + route.getDistance()
          + ";" + route.getCabType() + ";" + sysDateFormat.format(route.firstEmp().getPickTime()) + ";"
          + sysDateFormat.format(route.lastEmp().getDropTime()) + ";" + cab + ";" + route.getScore());
    }

		logger.debug("CabEvents-" + new Date() + ".txt", "UTF-8");

		logger.debug("Cab ID" + ";" + "Duty Num" + ";" + "Event Type" + ";" + "Event ID" + ";" + "Event Start Landmark"
				+ ";" + "Event Start" + ";" + "Event End Landmark" + ";" + "Event End" + ";" + "Start Time" + ";" + "End Time"
				+ ";" + "Dist traveled" + ";" + "Duty Hours" + ";" + "numTrips");

		for (Map<String, ActualCabPreferences> cabEntry : actualCabs.values()) {
			for (ActualCabPreferences cab : cabEntry.values()) {
				for (Event event : cab.getEvents()) {
					logger.debug(cab + ";" + event.getDuty() + ";" + event.getEventType() + ";" + event.getEventId() + ";"
							+ event.getStartLegLoc() + ";" + event.getStart() + ";" + event.getEndLegLoc() + ";" + event.getEnd()
							+ ";" + sysDateFormat.format(event.getEventStart()) + ";" + sysDateFormat.format(event.getEventEnd())
							+ ";" + event.getDistance() + ";" + cab.getDutyHours(event.getDuty()) + ";");
							//+ cab.getDutyWiseAllocatedRoutes().get(event.getDuty()).size());
				}
			}
		}

		logger.debug("lastKnownLoc-" + new Date() + ".txt", "UTF-8");

		logger.debug("Cab ID" + ";" + "Last Known Loc" + ";" + "Last Known Time");

		for (Map<String, ActualCabPreferences> cabEntry : actualCabs.values()) {
			for (ActualCabPreferences cab : cabEntry.values()) {
				if (cab.getLastKnownLoc() != null) {
					logger.debug(cab.getCabRegnNum() + ";" + cab.getLastKnownLoc() + ";"
							+ new SimpleDateFormat("dd/MM/yyyy HH:mm").format(cab.getLastKnownTimestamp()));

				}
			}
		}

		logger.debug("NewCabs-" + new Date() + ".txt", "UTF-8");

		logger.debug("Cab ID" + ";" + "Cab Type" + ";" + "Start Time" + ";" + "Start Threshold" + ";" + "Start Geocode"
				+ ";" + "End Time" + ";" + "End Threshold" + ";" + "End Buffer" + ";" + "End Geocode" + ";" + "Shifts" + ";"
				+ "Dist" + ";" + "Trips" + ";" + "Business" + ";" + "Number of Trips Given");

		for (Entry<Integer, Map<String, ActualCabPreferences>> cabEntry : actualCabs.entrySet()) {
			for (ActualCabPreferences cab : cabEntry.getValue().values()) {
				String cabPref = cab.getCabRegnNum() + ";" + cabEntry.getKey() + ";"
						+ new SimpleDateFormat("HH:mm").format(cab.getStartTime()) + ";" + cab.getStartTimeThreshold() + ";"
						+ cab.getStartLoc() + ";" + new SimpleDateFormat("HH:mm").format(cab.getEndTime()) + ";"
						+ cab.getEndTimeThreshold() + ";" + cab.getEndTimeExtn() + ";" + cab.getEndLoc() + ";" + "" + ";"
						+ cab.getMaxKmInDay() + ";" + cab.getMaxTripsInDay() + ";" + cab.getCabBusiness() + ";"
						+ cab.getAllocatedRoutes().size();
				logger.debug(cabPref);
			}
		}
		
		return resultList;

	}

	private void loadProperties() {

		Properties properties = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("/home/lohit/ws/autocaballocation/src/main/resources/FleetMix.properties");

			properties.load(input);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < 24; i++) {
			speedProfile.put(i, Double.parseDouble(properties.getProperty("speed_" + i)));
		}

		bufferTime = (int) (Integer.parseInt(properties.getProperty("bufferTime")) * (minutes));
		Double.parseDouble(properties.getProperty("maxDutyHours"));
		simulateLastKnownLoc = Boolean.parseBoolean(properties.getProperty("simulateLastKnownLoc"));
		maxIterationsToRun = Double.parseDouble(properties.getProperty("maxIterationsToRun"));

		Enumeration<String> enums = (Enumeration<String>) properties.propertyNames();
		while (enums.hasMoreElements()) {
			String key = enums.nextElement();
			if (key.contains("costPerKm")) {
				double cost = Double.parseDouble(properties.getProperty(key));

				String[] keyArray = key.split("_");
				String legType = keyArray[1];
				int cabCapacity = Integer.parseInt(keyArray[2]);

				Map<Integer, Double> cabCapCost = null;

				if (costPerKm.containsKey(legType)) {
					cabCapCost = costPerKm.get(legType);
					cabCapCost.put(cabCapacity, cost);
				} else {
					cabCapCost = new HashMap<Integer, Double>();
					cabCapCost.put(cabCapacity, cost);
					costPerKm.put(legType, cabCapCost);
				}
			} else if (key.contains("costPerTrip")) {
				double cost = Double.parseDouble(properties.getProperty(key));

				String[] keyArray = key.split("_");
				String legType = keyArray[1];
				int cabCapacity = Integer.parseInt(keyArray[2]);

				Map<Integer, Double> cabCapCost = null;

				if (costPerTrip.containsKey(legType)) {
					cabCapCost = costPerTrip.get(legType);
					cabCapCost.put(cabCapacity, cost);
				} else {
					cabCapCost = new HashMap<Integer, Double>();
					cabCapCost.put(cabCapacity, cost);
					costPerTrip.put(legType, cabCapCost);
				}
			} else if (key.contains("costPerWaitingMin")) {
				double cost = Double.parseDouble(properties.getProperty(key));

				String[] keyArray = key.split("_");
				int cabCapacity = Integer.parseInt(keyArray[1]);

				costPerWaitingMin.put(cabCapacity, cost);
			}
		}

	}

	//while using client to get shifts facing some deserialization issue
	private List<String> shiftDetail() {
		try {
			DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date shiftDate = new Date();
			String today = formatter.format(shiftDate);
			final String allShiftUrl = "http://tripbeta.moveinsync.com:9080/tripsheets/shiftdetail/businessunit/rideinsync-blr/date/"
					+ today;
			List shiftList = restTemplate.getForEntity(allShiftUrl, List.class).getBody();
			List<String> shifts = new ArrayList<String>();
			for (int i = 0; i < shiftList.size(); i++) {
				System.out.println(shiftList.get(i));
				shifts.add(((List) shiftList.get(i)).get(0).toString());
			}
			return shifts;
		} catch (Exception e2) {
			e2.printStackTrace();
			return null;
		}
	}

	private void loadRoutes(Map<String, List<String>> buidShiftMap) throws ParseException {
	  //20:0 (OUT)
//		List<String> shifts = shiftDetail();
		
		for (Map.Entry<String, List<String>> buidentry : buidShiftMap.entrySet()) {

		Map<String, List<TripsheetDTOV2>> shiftDetails = client.getTripsheetsByDateShifts(buidentry.getKey(), new Date(), buidentry.getValue());

		for(Map.Entry<String, List<TripsheetDTOV2>> entry : shiftDetails.entrySet()) { 
			String[] shiftParts = entry.getKey().split("-");
			String shift = entry.getKey().contains("IN") ? "Login ".concat(shiftParts[0]) : "Logout ".concat(shiftParts[0]);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
			String date = sdf.format(new Date());
			Date shiftTime = null;
			if (shift.contains("Login")) {
				shiftTime = new SimpleDateFormat("dd/mm/yyyy HH:mm")
						.parse(date + " " + shiftParts[0]);
			} else {
				shiftTime = new SimpleDateFormat("dd/mm/yyyy HH:mm")
						.parse(date + " " + shiftParts[0]);
			}

			for (int j = 0; j < entry.getValue().size(); j++) {
				TripsheetDTOV2 tripsheet = entry.getValue().get(j);
				List<EmployeeTripsheetDTOV2> employees = tripsheet.getEmployees();
				String routeId = String.valueOf(tripsheet.getTripId());
				String cabType = employees.size() <= 4 ? "4 s" : "6 s";
				double distance = tripsheet.getDistance();
				for (int k = 0; k < employees.size(); k++) {
					EmployeeTripsheetDTOV2 employee = employees.get(k);
					String empID = employee.getEmployeeId();
					Geocode pickUpGeoCode = new Geocode(employee.getPickupLoc().getGeoCord());
					Geocode dropGeoCode = new Geocode(employee.getDropLoc().getGeoCord());
					Date pickUpTime = employee.getPickupTime();
					Date dropTime = employee.getDropTime();
					String pickLandmark = employee.getPickupLoc().getLandmark();
					String dropLandmark = employee.getDropLoc().getLandmark();


					Route route = null;

					if (routes.containsKey(routeId)) {
						route = routes.get(routeId);
					} else {
						route = new Route();
						route.setRouteID(routeId);

						String direction = shift.split(" ")[0];
						route.setDirection(direction);


						route.setShift(shiftTime);

						route.setShiftTime(shift);

						route.setCabType(cabType);
						route.setDistance(distance);

						routes.put(routeId, route);
					}

					EmpTravelDetails travelDetails = new EmpTravelDetails();
					Employee emp = new Employee(empID);

					travelDetails.setEmp(emp);
					travelDetails.setPickLoc(pickUpGeoCode);
					travelDetails.setPickTime(pickUpTime);
					travelDetails.setDropLoc(dropGeoCode);
					travelDetails.setDropTime(dropTime);
					travelDetails.setPicLandmark(pickLandmark);
					travelDetails.setDropLandmark(dropLandmark);

					route.getEmpTravelDetails().add(travelDetails);

				}

			}
		}


		for (Entry<String, Route> routeEntry : routes.entrySet()) {
			Route route = routeEntry.getValue();
			Date shiftTime = route.getShift();
			String direction = route.getDirection();

			Map<Date, List<String>> routeMap = null;

			if (shiftWiseRoutes.containsKey(direction)) {

			} else {
				routeMap = new HashMap<Date, List<String>>();
				shiftWiseRoutes.put(direction, routeMap);
			}

			routeMap = shiftWiseRoutes.get(direction);

			List<String> routesInShift = null;

			if (routeMap.containsKey(shiftTime)) {

			} else {
				routesInShift = new ArrayList<String>();
				routeMap.put(shiftTime, routesInShift);
			}

			routesInShift = routeMap.get(shiftTime);
			routesInShift.add(route.getRouteID());
		}
		}

	}

  private void loadCabPreferences() throws JsonMappingException, JsonProcessingException, ParseException {

    List<FulfilmentResourceResponseDayWise> listOffulfilmentResource = fulfilmentResourceResponse
        .FulfilmentResourceResImp().stream().collect(Collectors.toList());
    System.out.println(listOffulfilmentResource);

    for (FulfilmentResourceResponseDayWise fulfilmentResourceResponseDayWise : listOffulfilmentResource) {

      // System.out.println(line);
      // String[] lineArray = line.split(";");
      ActualCabPreferences actCab = new ActualCabPreferences();

      String cabRegn = fulfilmentResourceResponseDayWise.getVehicleRegNo();
      actCab.setCabRegnNum(cabRegn);

      int cabCap = fulfilmentResourceResponseDayWise.getCapacity();
      //8:00 PM
      if (fulfilmentResourceResponseDayWise.getEntityAvailabilityDayWise().getStartTime() == null
          || fulfilmentResourceResponseDayWise.getEntityAvailabilityDayWise().getEndTime() == null) {
        continue;
      }
      
      String[] startTimeStrParts = fulfilmentResourceResponseDayWise.getEntityAvailabilityDayWise().getStartTime().split(" ");
      String startTimeStr ;
      if(startTimeStrParts[1].equals("PM")) {
        String[] timeParts = startTimeStrParts[0].split(":");
        startTimeStr = String.valueOf(Integer.valueOf(timeParts[0]) + 12).concat(":").concat(timeParts[1]).concat(":00");
      }else {
        String[] timeParts = startTimeStrParts[0].split(":");
        startTimeStr = Integer.valueOf(timeParts[0]) < 10 ? "0".concat(timeParts[0]).concat(":").concat(timeParts[1]).concat(":00"): startTimeStrParts[0].concat(":00");
      }
      
      String[] endTimeStrParts = fulfilmentResourceResponseDayWise.getEntityAvailabilityDayWise().getEndTime().split(" ");
      String endTimeStr ;
      if(endTimeStrParts[1].equals("PM")) {
        String[] timeParts = endTimeStrParts[0].split(":");
        endTimeStr = String.valueOf(Integer.valueOf(timeParts[0]) + 12).concat(":").concat(timeParts[1]).concat(":00");
      }else {
        String[] timeParts = endTimeStrParts[0].split(":");
        endTimeStr = Integer.valueOf(timeParts[0]) < 10 ? "0".concat(timeParts[0]).concat(":").concat(timeParts[1]).concat(":00"): endTimeStrParts[0].concat(":00");
      }
      
      Date startTime = new SimpleDateFormat("HH:mm:ss")
          .parse(startTimeStr);
      actCab.setStartTime(startTime);

      int startTimeThreshold = Integer.parseInt("30");
      actCab.setStartTimeThreshold(startTimeThreshold);
      
      if(!fulfilmentResourceResponseDayWise.getGarageGeoCord().contains(",") || !fulfilmentResourceResponseDayWise.getEndGarageGeoCord().contains(",")) {
        continue;
      }

      Geocode startGeoCode = new Geocode(fulfilmentResourceResponseDayWise.getGarageGeoCord());
      actCab.setStartLoc(startGeoCode);

      Date endTime = new SimpleDateFormat("HH:mm:ss")
          .parse(endTimeStr);
      actCab.setEndTime(endTime);

      int endTimeThreshold = Integer.parseInt("30");
      actCab.setEndTimeThreshold(endTimeThreshold);

      int endTimeExtn = Integer.parseInt("0");
      actCab.setEndTimeExtn(endTimeExtn);

      Geocode endGeoCode = new Geocode(fulfilmentResourceResponseDayWise.getEndGarageGeoCord());
      actCab.setEndLoc(endGeoCode);

//      String[] shiftArray = "Logout 16:00,Login 05:15,Logout 02:30,Login 06:15,Logout 15:00,Logout 15:30,Login 06:45,Logout 21:30,Logout 22:30,Login 12:15,Login 13:15,Logout 01:30,Login 13:45,Logout 23:00,Login 17:15,Login 18:15,Logout 03:30,Login 21:45,Logout 07:00,Logout 14:30,Login 07:15,Logout 02:00,Logout 16:30,Logout 22:00,Login 16:15,Login 14:15,Logout 13:30,Logout 17:30,Logout 06:30,Logout 00:30,Login 21:15,Logout 05:30,Login 06:00,Login 13:30,Login 11:00,Logout 20:00,Login 13:00,Login 09:00,Login 21:00,Logout 18:00,Logout 06:00,Login 05:00,Logout 14:00,Login 12:00,Login 10:00,Login 08:00,Login 07:00,Login 14:00,Login 22:30,Logout 07:30,Logout 17:45,Logout 19:30,Logout 01:00,Logout 00:07"
//          .split(",");
//      for (int i = 0; i < shiftArray.length; i++) {
//        actCab.addShift(shiftArray[i]);
//      }

      double maxKmInDay = Double.parseDouble("160");
      actCab.setMaxKmInDay(maxKmInDay);

      int maxTripsInDay = Integer.parseInt("6");
      actCab.setMaxTripsInDay(maxTripsInDay);

      int get2WorkCab = Integer.parseInt("0");
      actCab.setCabBusiness(get2WorkCab);

      if (actualCabs.containsKey(cabCap)) {
        actualCabs.get(cabCap).put(cabRegn, actCab);
      } else {
        Map<String, ActualCabPreferences> actCabs = new HashMap<String, ActualCabPreferences>();
        actCabs.put(cabRegn, actCab);
        actualCabs.put(cabCap, actCabs);
      }

    }

  }



	private void loadCabDeployment(Map<String, List<String>> buidShiftMap) {
		// Route ID;Cab Regn Number;Start Time;End Time;Distance;Start Location;End
		// Location;Start Landmark;End Landmark
	  Map<String, Integer> routeIdMap = new HashMap<String, Integer>();
      int i = 0;
      
	    for (Map.Entry<String, List<String>> buidentry : buidShiftMap.entrySet()) {
	      
	      Map<String, List<TripsheetDTOV2>> tripsheets = client.getTripsheetsByDateShifts(buidentry.getKey(), new Date(), buidentry.getValue());

	        for(Map.Entry<String, List<TripsheetDTOV2>> entry : tripsheets.entrySet()) {

	            for (int k = 0; k < entry.getValue().size(); k++) {
	                TripsheetDTOV2 tripsheet = entry.getValue().get(k);
	                if(tripsheet.getCab() == null) {
	                    continue;
	                }
	                String routeId = String.valueOf(tripsheet.getTripId());
	                double distance = tripsheet.getDistance();
	                String cabRegnNum = tripsheet.getCab().getRegistration();

	                ActualCabPreferences actCab = null;

	                for (int cabCap : actualCabs.keySet()) {
	                    if (actualCabs.get(cabCap).containsKey(cabRegnNum)) {
	                        actCab = actualCabs.get(cabCap).get(cabRegnNum);
	                        break;
	                    }
	                }

	                if (actCab == null) {
	                    continue;
	                }

	                Route route = null;

	                if (otherRoutes.containsKey(routeId)) {
	                    route = otherRoutes.get(routeId);
	                    int currentMaxRoutes = 0;

	                    if (routeIdMap.containsKey(routeId)) {
	                        currentMaxRoutes = routeIdMap.get(routeId);
	                    }

	                    if (route.getActualAllocatedCab().getCabRegnNum().equals(cabRegnNum)) {
	                        // do nothing
	                    } else {
	                        boolean foundCabRoute = false;
	                        for (int j = 1; j < currentMaxRoutes; j++) {
	                            route = otherRoutes.get(routeId + "-" + j);
	                            if (route.getActualAllocatedCab().getCabRegnNum().equals(cabRegnNum)) {
	                                foundCabRoute = true;
	                                break;
	                            }
	                        }
	                        if (!foundCabRoute) {
	                            currentMaxRoutes++;
	                            routeIdMap.put(routeId, currentMaxRoutes);

	                            route = new Route();
	                            route.setRouteID(routeId + "-" + currentMaxRoutes);

	                            route.setDirection("");

	                            route.setDistance(distance);

	                            otherRoutes.put(routeId, route);
	                        }
	                    }
	                } else {
	                    route = new Route();
	                    route.setRouteID(routeId);

	                    route.setDirection("");

	                    route.setDistance(distance);

	                    otherRoutes.put(routeId, route);
	                }

	                EmpTravelDetails travelDetails = new EmpTravelDetails();
	                Employee emp = new Employee("" + (i++));

	                if(tripsheet.getEmployees().size()>0) {
	                    String pickupGeocode = tripsheet.getEmployees().get(0).getPickupLoc().getGeoCord();
	                    String dropGeocode = tripsheet.getEmployees().get(tripsheet.getEmployees().size()-1).getPickupLoc().getGeoCord();
	                    travelDetails.setPickLoc(new Geocode(pickupGeocode));
	                    travelDetails.setDropLoc(new Geocode(dropGeocode));
	                }


	                travelDetails.setEmp(emp);
	                travelDetails.setPickTime(tripsheet.getActualStartTime());
	                travelDetails.setDropTime(tripsheet.getActualEndTime());
	                travelDetails.setPicLandmark("Trip Start");
	                travelDetails.setDropLandmark("Trip End");

	                route.getEmpTravelDetails().add(travelDetails);
	                /*
	                 * actCab.addRoute(route, 2);
	                 */
	                if (actCab.getCurrentDuty() == 0) {
	                    actCab.addDuty();
	                }

	                route.setActualAllocatedCab(actCab, 2);

	            }

	        }
          
        }

//		List<String> shifts = shiftDetail();
		
		

	}

	private static void simulateLastKnownLoc() {
		String fileName = "lastKnownLoc";

		BufferedReader br = null;
		String line = "";
		try {
			br = new BufferedReader(new FileReader(fileName));
			int i = 0;
			while ((line = br.readLine()) != null) {
				// System.out.println(line);
				if (line.equals("Cab ID;Last Known Loc;Last Known Time")) {
					continue;
				}

				String[] lineArray = line.split(";");

				String cabRegnNum = lineArray[0];

				String lastKnownLocString = lineArray[1];

				if (lastKnownLocString == "null" || lastKnownLocString == "Last Known Loc") {
					continue;
				}

				Geocode lastKnownLoc = new Geocode(lastKnownLocString);
				Date lastKnownTimestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(lineArray[2]);

				ActualCabPreferences actCab = null;

				for (int cabCap : actualCabs.keySet()) {
					if (actualCabs.get(cabCap).containsKey(cabRegnNum)) {
						actCab = actualCabs.get(cabCap).get(cabRegnNum);
						break;
					}
				}

				if (actCab == null) {
					continue;
				}

				actCab.setLastKnownLoc(lastKnownLoc);
				actCab.setLastKnownTimestamp(lastKnownTimestamp);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}

	// public String getLasrloc() {
	// HttpHeaders headers = new HttpHeaders();
	// headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
	// HttpEntity <String> entity = new HttpEntity<String>(headers);
	// String url="https://ds.moveinsync.com/ds/location/last-location/registrations";
	// return restTemplate.exchange(url, HttpMethod.GET, entity, String.class).getBody();
	// // assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
	// }
	//

	private static void getLastKnownLoc() {
		/*
		 * RequestConfig.Builder requestBuilder = RequestConfig.custom(); requestBuilder =
		 * requestBuilder.setConnectTimeout(0); requestBuilder = requestBuilder.setConnectionRequestTimeout(0);
		 * requestBuilder = requestBuilder.setSocketTimeout(0);
		 * 
		 * HttpClientBuilder builder = HttpClientBuilder.create(); builder.setDefaultRequestConfig(requestBuilder.build());
		 */
		DefaultHttpClient client = new DefaultHttpClient();// builder.build();
		HttpPost post = new HttpPost("https://ds.moveinsync.com/ds/location/last-location/registrations");
		post.setHeader("Content-Type", "application/json");

		String request = "[";

		for (Entry<Integer, Map<String, ActualCabPreferences>> actCabEntrySet : actualCabs.entrySet()) {
			// registrations.addAll(actCabEntrySet.getValue().keySet());
			for (String cabRegnNum : actCabEntrySet.getValue().keySet()) {
				request = request + "\"" + cabRegnNum + "\",";
			}
		}

		request = request.substring(0, request.length() - 1) + "]";

		try {
			StringEntity payload = new StringEntity(request);
			post.setEntity(payload);
			HttpResponse response = client.execute(post);
			// System.out.println("GetLastLocations: ResponseCode " + response.getStatusLine().getStatusCode());

			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Error calling last known location API: " + response.getStatusLine().getStatusCode());
			}

			String result = EntityUtils.toString(response.getEntity());
			Gson gson = new Gson();
			LastLocation lastLocation = gson.fromJson(result, LastLocation.class);

			Map<String, List<String>> cabLocationMap = lastLocation.getCabLocationMap();

			for (Entry<String, List<String>> actCabEntrySet : cabLocationMap.entrySet()) {
				String cabRegNum = actCabEntrySet.getKey();
				// System.out.println(location.get(0) + " " + location.get(1));
				Geocode location = new Geocode(actCabEntrySet.getValue().get(0));
				Date timeStamp = new Date(Long.parseLong(actCabEntrySet.getValue().get(1)));
				ActualCabPreferences cab = null;
				for (Integer capacity : actualCabs.keySet()) {
					Map<String, ActualCabPreferences> cabMap = actualCabs.get(capacity);
					if (cabMap.containsKey(cabRegNum)) {
						cab = cabMap.get(cabRegNum);
					}
				}

				if (cab != null) {
					cab.setLastKnownLoc(location);
					cab.setLastKnownTimestamp(timeStamp);
				}
			}

			// System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private static void simulateCurrentTravelPlan() {
		for (Map<String, ActualCabPreferences> cabMap : actualCabs.values()) {

			for (ActualCabPreferences cab : cabMap.values()) {
				List<Event> events = new ArrayList<Event>();
				events.addAll(cab.getEvents());

				Collections.sort(events);

				Event lastEvent = null;

				for (Event event : events) {
					int duty = event.getDuty();
					if (lastEvent == null) {
						double emptyLegDist = Utils.getAerialDist(cab.getStartLoc(), event.getStart());
						double speed = getSpeed(event.getEventStart());
						long travelTime = (long) (event.getEventStart().getTime() - (emptyLegDist / speed) * (hours));

						Date homeLeaveTime = new Date(travelTime);
						cab.addEmptyLeg(emptyLegDist, homeLeaveTime, event.getEventStart(), cab.getStartLoc(), event.getStart(),
								"Start Garage", event.getStartLegLoc(), -1, duty);

					} else {
						double emptyLegDist = Utils.getAerialDist(lastEvent.getEnd(), event.getStart());
						double speed = getSpeed(event.getEventStart());
						long travelTime = (long) (event.getEventStart().getTime() - (emptyLegDist / speed) * (hours));

						Date homeLeaveTime = new Date(travelTime);

						if (homeLeaveTime.before(lastEvent.getEventEnd())) {
							// System.out.println("Cab Allocation looks problematic " + lastEvent.getEventId() + " to "
							// + event.getEventId());
							event.setComments(
									"Cab Allocation looks problematic " + lastEvent.getEventId() + " to " + event.getEventId());
						}

						cab.addEmptyLeg(emptyLegDist, homeLeaveTime, event.getEventStart(), lastEvent.getEnd(), event.getStart(),
								lastEvent.getEndLegLoc(), event.getStartLegLoc(), -1, duty);

					}

					lastEvent = event;
				}

				if (lastEvent == null) {
					continue;
				}

				double lastLegDist = Utils.getAerialDist(lastEvent.getEnd(), cab.getEndLoc());
				double speed = getSpeed(lastEvent.getEventEnd());
				long travelTime = (long) (lastEvent.getEventEnd().getTime() + (lastLegDist / speed) * (hours));

				Date homeReachTime = new Date(travelTime);

				cab.addEmptyLeg(lastLegDist, lastEvent.getEventEnd(), homeReachTime, lastEvent.getEnd(), cab.getEndLoc(),
						lastEvent.getEndLegLoc(), "End Garage", -1, lastEvent.getDuty());
			}
		}

	}

	private static double getSpeed(Date shift) {
		double speed = 20.0;
		speed = speedProfile.get(shift.getHours());
		return speed;
	}

	private static void allocateCabs() {

		List<Route> allRoutes = new ArrayList<Route>(routes.values());
		Utils.sortRouteByStartTime(allRoutes);

		List<String> routeIds = new ArrayList<String>();
		Date startTime = new Date();

		for (Route route : allRoutes) {
			if (route.getAllocationStartTime().equals(startTime)) {
				routeIds.add(route.getRouteID());
			} else {
				startTime = route.getAllocationStartTime();
				if (!routeIds.isEmpty()) {
					allocateFreshCabs(routeIds);
				}

				routeIds = new ArrayList<String>();
				routeIds.add(route.getRouteID());
			}
		}

		if (!routeIds.isEmpty()) {
			allocateFreshCabs(routeIds);
		}
	}

	private static void allocateFreshCabs(List<String> routeIds) {
		Map<Route, Map<ActualCabPreferences, Double>> mapCabRoutes = new HashMap<Route, Map<ActualCabPreferences, Double>>();
		batch++;

		System.out.println(numIter + " " + batch + " " + sdf.format(routes.get(routeIds.get(0)).getAllocationStartTime())
		+ " " + routeIds.size());

		int oldCabsFound = 0;
		int oldCabsButNewUsed = 0;
		int newCabs = 0;

		for (String routeId : routeIds) {
			Route route = routes.get(routeId);

			if (route.getAllocatedCab() == null) {

				if (route.getRouteID().equals("attblr-928109")) {
					// System.out.println("81082");
				}
				int cabCapacity = Integer.parseInt(route.getCabType().split(" ")[0]);

				Map<ActualCabPreferences, Double> cabScores = getCabs(cabCapacity, route, false);

				if (cabScores.keySet().size() > 0) {
					mapCabRoutes.put(route, cabScores);
				} else {

					if (numIter > 0) {
						// System.out.println(numIter + " No cab For Route " + route);
					}

					// no cab can do the trip
					ActualCabPreferences newCab = addNewCab(route);
					allocateCabToRoute(newCab, route, 100);
					newCabs++;
				}
			}
		}

		// List<ActualCabPreferences> cabsNotConsideredForAlloc = new ArrayList<ActualCabPreferences>();
		// cabsNotConsideredForAlloc.addAll(actualCabs.get(4).values());
		//
		// for (Route route : mapCabRoutes.keySet()) {
		// for (ActualCabPreferences cab : mapCabRoutes.get(route).keySet()) {
		// if (cabsNotConsideredForAlloc.contains(cab)) {
		// cabsNotConsideredForAlloc.remove(cab);
		// }
		// }
		// }
		//
		// PrintWriter writer2 = null;
		// try {
		// writer2 = new PrintWriter("CabsNotConsideredForAlloc-.txt", "UTF-8");
		// } catch (FileNotFoundException e) {
		// e.printStackTrace();
		// } catch (UnsupportedEncodingException e) {
		// e.printStackTrace();
		// }
		//
		// writer2.println("Cab");
		// for (ActualCabPreferences cab : cabsNotConsideredForAlloc) {
		// writer2.println(cab.getCabRegnNum());
		// }
		// writer2.flush();
		// writer2.close();

		Map<Route, Map<ActualCabPreferences, Double>> mapCabRoutesBeforeElimination = (Map<Route, Map<ActualCabPreferences, Double>>) ((HashMap<Route, Map<ActualCabPreferences, Double>>) mapCabRoutes)
				.clone();

		while (mapCabRoutes.keySet().size() > 0) {
			double bestScore = 0 - Double.MAX_VALUE;
			Route bestRoute = null;
			boolean lessRoute = false;
			ActualCabPreferences bestCab = null;

			for (Route route : mapCabRoutes.keySet()) {
				// if(mapCabRoutes.get(route).keySet().size() <=1) {
				// if(lessRoute) {
				//
				// } else {
				// bestScore = 0 - Double.MAX_VALUE;
				// lessRoute = true;
				// }
				//
				// } else {
				// if(lessRoute) {
				// continue;
				// }
				// }
				for (Entry<ActualCabPreferences, Double> entry : mapCabRoutes.get(route).entrySet()) {
					if (entry.getValue() > bestScore) {
						bestScore = entry.getValue();
						bestRoute = route;
						bestCab = entry.getKey();
					}

					if (entry.getValue() == bestScore) {
						if (route == bestRoute) {

						}
					}
				}
			}

			if (bestRoute == null) {
				if (numIter > 0) {
					// System.out.println(numIter + " " + "No Best Route " + mapCabRoutes);
				}
				oldCabsButNewUsed = oldCabsButNewUsed + mapCabRoutes.keySet().size();
				addNewCabsForRemainingRoutes(mapCabRoutes);
				System.out.println(routeIds.size() + " " + oldCabsFound + " " + oldCabsButNewUsed + " " + newCabs);
				logger.debug(numIter + ";" + batch + ";" + sdf.format(routes.get(routeIds.get(0)).getAllocationStartTime())
				+ ";" + routeIds.size() + ";" + oldCabsFound + ";" + oldCabsButNewUsed + ";" + newCabs);
				return;
			}

			if (bestRoute.getAllocationStartTime().getHours() > 9 && bestCab.getCurrentDuty() == 0
					&& prevIterAlloc.containsKey(bestRoute) && prevIterAlloc.get(bestRoute) != bestCab) {
				// System.out.println("test");
			}

			if (prevIterAlloc != null && prevIterAlloc.containsKey(bestRoute) && prevIterAlloc.get(bestRoute) != bestCab) {
				cabChanges++;
				// System.out.println("Changing Cab");
			}
			oldCabsFound++;
			allocateCabToRoute(bestCab, bestRoute, bestScore);

			mapCabRoutes.remove(bestRoute);

			for (Route route : mapCabRoutes.keySet()) {
				if (mapCabRoutes.get(route).containsKey(bestCab)) {
					mapCabRoutes.get(route).remove(bestCab);
				}
			}

		}
		logger.debug(numIter + ";" + batch + ";" + sdf.format(routes.get(routeIds.get(0)).getAllocationStartTime()) + ";"
				+ routeIds.size() + ";" + oldCabsFound + ";" + oldCabsButNewUsed + ";" + newCabs);
		System.out.println(routeIds.size() + " " + oldCabsFound + " " + oldCabsButNewUsed + " " + newCabs);
	}

	private static void allocateCabToRoute(ActualCabPreferences bestCab, Route bestRoute, double score) {

		if (bestCab.getCabRegnNum().equals("New-0.0-1")) {
			// System.out.println("test");
		}

		if (bestRoute.getRouteID().equals("whatfixblr-5732")) {
			// System.out.println("test");
		}

		bestRoute.setScore(score);

		int duty = bestCab.findDutyForRoute(bestRoute.getStartTime());

		boolean firstTripOfDuty = (bestCab.getAllocatedRoutes().containsKey(duty) && bestCab.numTripsInDuty(duty) == 0)
				|| (!bestCab.getAllocatedRoutes().containsKey(duty));
		if (firstTripOfDuty && (bestRoute.getStartTime().getTime()
				- bestCab.getCabDutyStartTimeForRoute(bestRoute.getStartTime()).getTime() > (3 * hours))) {
			// System.out.println("first trip too late");
			// Map<ActualCabPreferences, Double> possibleCabs = new HashMap<ActualCabPreferences, Double>();
			// getScore(bestRoute, bestCab, possibleCabs);
		}

		bestRoute.setActualAllocatedCab(bestCab, 1);
		duty = bestCab.findDutyForRoute(bestRoute.getStartTime());

		List<Event> cabEvents = bestCab.getDutyWiseEvents().get(duty);

		if (bestCab.getCabRegnNum().equals("KA-05-AF-9852") && bestRoute.getRouteID().equalsIgnoreCase("99109")) {
			// System.out.println("KA-51-AD-9772");
		}

		Event lastKnownTrip = null;
		Event nextKnownTrip = null;
		Event lastKnownLeg = null;
		Event nextKnownLeg = null;

		if (cabEvents == null) {

		} else {
			Map<String, Event> events = fetchLastAndNextCabEvents(cabEvents, lastKnownTrip, nextKnownTrip, bestRoute);
			lastKnownTrip = events.get("last");
			nextKnownTrip = events.get("next");
		}

		String startLoc;
		String endLoc;

		double emptyLegDist;

		Date startTime;
		Date endTime;

		Geocode start;
		Geocode end;

		if (lastKnownTrip == null) {
			if (nextKnownTrip == null) {

				boolean useLastKnownLoc = false;
				Geocode lastKnownLocation = null;
				Date lastKnownTimestamp = null;

				if (bestCab.getLastKnownLoc() != null) {
					lastKnownLocation = bestCab.getLastKnownLoc();
					lastKnownTimestamp = bestCab.getLastKnownTimestamp();

					if (bestRoute.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)) {
						useLastKnownLoc = true;
					}
				}

				/*
				 * if (bestCab.getCurrentDuty() == 0) { bestCab.addDuty(); }
				 */

				// bestCab.addRoute(bestRoute);
				// bestRoute.setActualAllocatedCab(bestCab, 1);

				start = bestCab.getStartLoc();
				end = bestRoute.firstEmp().getPickLoc();

				startLoc = "Start Garage";
				endLoc = bestRoute.firstEmp().getPicLandmark();

				if (useLastKnownLoc) {
					start = lastKnownLocation;
					startLoc = "Last Known Location (" + sysDateFormat.format(lastKnownTimestamp) + ")";
				}

				emptyLegDist = Utils.getAerialDist(start, end);
				endTime = bestRoute.getStartTime();

				double speed = getSpeed(endTime);
				startTime = new Date((long) (endTime.getTime() - ((emptyLegDist / speed) * (hours))));

				bestCab.addEmptyLeg(emptyLegDist, startTime, endTime, start, end, startLoc, endLoc, 0, duty);

				double emptyLegDist2 = Utils.getAerialDist(bestRoute.getEndLoc(), bestCab.getEndLoc());
				double speed2 = getSpeed(bestCab.getEndTime());
				double travelTime2 = bestRoute.getEndTime().getTime() + ((emptyLegDist2 / speed2) * (hours));

				Date endTime2 = new Date((long) travelTime2);

				bestCab.addEmptyLeg(emptyLegDist2, bestRoute.getEndTime(), endTime2, bestRoute.getEndLoc(), bestCab.getEndLoc(),
						bestRoute.getEndLandmark(), "End Garage", 0, duty);

			} else {
				/*
				 * List<Event> toRemove = new ArrayList<Event>();
				 * 
				 * for (Event event : cabEvents) { if (event.getEventEnd().before(nextKnownTrip.getEventStart())) {
				 * toRemove.add(event); } }
				 * 
				 * for(Event event : toRemove) { cabEvents.remove(event); }
				 */

				boolean useLastKnownLoc = false;
				Geocode lastKnownLocation = null;
				Date lastKnownTimestamp = null;

				if (bestCab.getLastKnownLoc() != null) {
					lastKnownLocation = bestCab.getLastKnownLoc();
					lastKnownTimestamp = bestCab.getLastKnownTimestamp();

					if (bestRoute.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)) {
						useLastKnownLoc = true;
					}
				}

				if (bestCab.getCurrentDuty() == 0) {
					bestCab.addDuty();
				}

				// bestCab.addRoute(bestRoute);
				// bestRoute.setActualAllocatedCab(bestCab, 1);

				start = bestCab.getStartLoc();
				end = bestRoute.getStartLoc();

				startLoc = "Start Garage";
				endLoc = bestRoute.firstEmp().getPicLandmark();

				if (useLastKnownLoc) {
					start = lastKnownLocation;
					startLoc = "Last Known Location (" + sysDateFormat.format(lastKnownTimestamp) + ")";
				}

				emptyLegDist = Utils.getAerialDist(start, end);
				endTime = bestRoute.getStartTime();

				double speed = getSpeed(endTime);
				startTime = new Date((long) (endTime.getTime() - ((emptyLegDist / speed) * (hours))));

				bestCab.addEmptyLeg(emptyLegDist, startTime, endTime, start, end, startLoc, endLoc, 0, duty);

				Geocode start2 = bestRoute.getEndLoc();
				Geocode end2 = nextKnownTrip.getStart();

				String startLoc2 = bestRoute.getEndLandmark();
				String endLoc2 = nextKnownTrip.getStartLegLoc();

				double emptyLegDist2 = Utils.getAerialDist(start, end);
				Date endTime2 = nextKnownTrip.getEventStart();

				double speed2 = getSpeed(endTime2);
				Date startTime2 = new Date((long) (endTime2.getTime() - ((emptyLegDist2 / speed2) * (hours))));

				for (Event event : cabEvents) {
					if (event.getEventType() <= 0) {
						if (event.getEventStart().before(nextKnownTrip.getEventStart())) {
							if (nextKnownLeg == null) {
								nextKnownLeg = event;
							} else {
								if (event.getEventStart().after(nextKnownLeg.getEventStart())) {
									nextKnownLeg = event;
								}
							}
						}
					}
				}

				if (nextKnownLeg == null) {
					bestCab.addEmptyLeg(emptyLegDist2, startTime2, endTime2, start2, end2, startLoc2, endLoc2, 0, duty);
				} else {
					nextKnownLeg.setDistance(emptyLegDist2);
					nextKnownLeg.setEventStart(startTime2);
					nextKnownLeg.setEventEnd(endTime2);
					nextKnownLeg.setStart(start2);
					nextKnownLeg.setEnd(end2);
					nextKnownLeg.setStartLegLoc(startLoc2);
					nextKnownLeg.setEndLegLoc(endLoc2);
				}
			}
		} else {
			if (nextKnownTrip == null) {
				/*
				 * List<Event> toRemove = new ArrayList<Event>(); for (Event event : cabEvents) { if
				 * (event.getEventStart().after(lastKnownTrip.getEventEnd())) { toRemove.add(event); } }
				 * 
				 * for(Event event : toRemove) { cabEvents.remove(event); }
				 */
				// bestCab.addRoute(bestRoute);
				boolean useLastKnownLoc = false;
				Geocode lastKnownLocation = null;
				Date lastKnownTimestamp = null;

				if (bestCab.getLastKnownLoc() != null) {
					lastKnownLocation = bestCab.getLastKnownLoc();
					lastKnownTimestamp = bestCab.getLastKnownTimestamp();

					if (bestRoute.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)) {
						useLastKnownLoc = true;
					}
				}

				// bestRoute.setActualAllocatedCab(bestCab, 1);

				start = lastKnownTrip.getEnd();
				end = bestRoute.getStartLoc();

				startLoc = lastKnownTrip.getEndLegLoc();
				endLoc = bestRoute.firstEmp().getPicLandmark();

				if (useLastKnownLoc) {
					start = lastKnownLocation;
					startLoc = "Last Known Location (" + sysDateFormat.format(lastKnownTimestamp) + ")";
				}

				emptyLegDist = Utils.getAerialDist(start, end);
				endTime = bestRoute.getStartTime();

				double speed = getSpeed(endTime);
				startTime = new Date((long) (endTime.getTime() - ((emptyLegDist / speed) * (hours))));

				for (Event event : cabEvents) {
					if (event.getEventType() <= 0) {
						if (event.getEventStart().after(lastKnownTrip.getEventStart())) {
							if (lastKnownLeg == null) {
								lastKnownLeg = event;
							} else {
								if (event.getEventStart().before(lastKnownLeg.getEventStart())) {
									lastKnownLeg = event;
								}
							}
						}
					}
				}

				if (lastKnownLeg == null) {
					bestCab.addEmptyLeg(emptyLegDist, startTime, endTime, start, end, startLoc, endLoc, 0, duty);
				} else {
					lastKnownLeg.setDistance(emptyLegDist);
					lastKnownLeg.setEventStart(startTime);
					lastKnownLeg.setEventEnd(endTime);
					lastKnownLeg.setStart(start);
					lastKnownLeg.setEnd(end);
					lastKnownLeg.setStartLegLoc(startLoc);
					lastKnownLeg.setEndLegLoc(endLoc);
				}

				double emptyLegDist2 = Utils.getAerialDist(bestRoute.getEndLoc(), bestCab.getEndLoc());
				double speed2 = getSpeed(bestCab.getEndTime());
				double travelTime2 = bestRoute.getEndTime().getTime() + ((emptyLegDist2 / speed2) * (hours));

				Date endTime2 = new Date((long) travelTime2);

				bestCab.addEmptyLeg(emptyLegDist2, bestRoute.getEndTime(), endTime2, bestRoute.getEndLoc(), bestCab.getEndLoc(),
						bestRoute.getEndLandmark(), "End Garage", 0, duty);
			} else {
				/*
				 * List<Event> toRemove = new ArrayList<Event>(); for (Event event : cabEvents) { if
				 * (event.getEventEnd().before(nextKnownTrip.getEventStart()) &&
				 * event.getEventStart().after(lastKnownTrip.getEventEnd())) { toRemove.add(event); } }
				 * 
				 * for(Event event : toRemove) { cabEvents.remove(event); }
				 */
				// bestCab.addRoute(bestRoute);
				// bestRoute.setActualAllocatedCab(bestCab, 1);

				for (Event event : cabEvents) {
					if (event.getEventType() <= 0) {
						if (event.getEventStart().before(nextKnownTrip.getEventStart())) {
							if (nextKnownLeg == null) {
								if (event.getEnd().equals(nextKnownTrip.getStart())) {
									nextKnownLeg = event;
								}
							} else {
								if (event.getEventStart().after(nextKnownLeg.getEventStart())) {
									if (event.getEnd().equals(nextKnownTrip.getStart())) {
										nextKnownLeg = event;
									}
								}
							}
						}
					}
				}

				start = lastKnownTrip.getEnd();
				end = bestRoute.getStartLoc();

				startLoc = lastKnownTrip.getEndLegLoc();
				endLoc = bestRoute.firstEmp().getPicLandmark();

				boolean useLastKnownLoc = false;
				Geocode lastKnownLocation = null;
				Date lastKnownTimestamp = null;

				if (bestCab.getLastKnownLoc() != null) {
					lastKnownLocation = bestCab.getLastKnownLoc();
					lastKnownTimestamp = bestCab.getLastKnownTimestamp();

					if (bestRoute.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)) {
						useLastKnownLoc = true;
						start = lastKnownLocation;
						startLoc = "Last Known Location (" + sysDateFormat.format(lastKnownTimestamp) + ")";
					}
				}

				emptyLegDist = Utils.getAerialDist(start, end);
				endTime = bestRoute.getStartTime();

				double speed = getSpeed(endTime);
				startTime = new Date((long) (endTime.getTime() - ((emptyLegDist / speed) * (hours))));

				bestCab.addEmptyLeg(emptyLegDist, startTime, endTime, start, end, startLoc, endLoc, 0, duty);

				Geocode start2 = bestRoute.getEndLoc();
				Geocode end2 = nextKnownTrip.getStart();

				String startLoc2 = bestRoute.getEndLandmark();
				String endLoc2 = nextKnownTrip.getStartLegLoc();

				double emptyLegDist2 = Utils.getAerialDist(start, end);
				Date endTime2 = nextKnownTrip.getEventStart();

				double speed2 = getSpeed(endTime2);
				Date startTime2 = new Date((long) (endTime2.getTime() - ((emptyLegDist2 / speed2) * (hours))));

				if (nextKnownLeg == null) {
					bestCab.addEmptyLeg(emptyLegDist2, startTime2, endTime2, start2, end2, startLoc2, endLoc2, 0, duty);
				} else {
					nextKnownLeg.setDistance(emptyLegDist2);
					nextKnownLeg.setEventStart(startTime2);
					nextKnownLeg.setEventEnd(endTime2);
					nextKnownLeg.setStart(start2);
					nextKnownLeg.setEnd(end2);
					nextKnownLeg.setStartLegLoc(startLoc2);
					nextKnownLeg.setEndLegLoc(endLoc2);
				}

			}
		}

	}

	private static Map<String, Event> fetchLastAndNextCabEvents(List<Event> cabEvents, Event lastKnownEvent,
			Event nextKnownEvent, Route route) {
		Map<String, Event> events = new HashMap<String, Event>();
		for (Event event : cabEvents) {
			if (event.getEventType() <= 0) {
				continue;
			}

			if (event.getEventId().equalsIgnoreCase(route.getRouteID())) {
				continue;
			}

			if (event.getEventStart().before(route.getStartTime()) || event.getEventStart().equals(route.getStartTime())) {
				if (lastKnownEvent == null) {
					lastKnownEvent = event;
				} else {
					if (lastKnownEvent.getEventStart().before(event.getEventStart())) {
						lastKnownEvent = event;
					}
				}
			}

			if (event.getEventStart().after(route.getStartTime())) {
				if (nextKnownEvent == null) {
					nextKnownEvent = event;
				} else {
					if (nextKnownEvent.getEventStart().after(event.getEventStart())) {
						nextKnownEvent = event;
					}
				}
			}
		}
		events.put("last", lastKnownEvent);
		events.put("next", nextKnownEvent);
		return events;
	}

	private static Map<ActualCabPreferences, Double> getCabs(int cabCapacity, Route route, boolean relaxed) {
		Map<ActualCabPreferences, Double> possibleCabs = new HashMap<ActualCabPreferences, Double>();

		if (actualCabs.containsKey(cabCapacity)) {
			Set<ActualCabPreferences> applicableCabs = new HashSet<ActualCabPreferences>(
					actualCabs.get(cabCapacity).values());
			for (ActualCabPreferences cab : applicableCabs) {

				if (!routeWithinCabTime(route, cab)) {
					continue;
				}

				possibleCabs = getScore(route, cab, possibleCabs);

			}
		}

		return possibleCabs;
	}

	private static Map<ActualCabPreferences, Double> getScore(Route route, ActualCabPreferences cab,
			Map<ActualCabPreferences, Double> possibleCabs) {
		boolean newDutyForCab = false;

		int duty = cab.getCurrentDuty();

		if (duty > 0) {
			duty = cab.findDutyForRoute(route.getStartTime());
		}

		if (duty == -1) {
			newDutyForCab = true;
		}

		if (newDutyForCab || cab.getCurrentDuty() == 0 || cab.numTripsInDuty(duty) == 0) {
			FeasibilityObject feasibilityOfFirstTrip = checkFeasibilityOfFirstTripOfDuty(cab, route, possibleCabs, null, null,
					false, duty);

			if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfFirstTrip.isFeasible()) {
				// System.out.println("Not feasible");
			}
			logFeasibilityOfTrip(feasibilityOfFirstTrip, cab, route, possibleCabs, null, null,
					"Trip Not Feasible as the First Trip for Cab");
		} else {
			// Its not the first duty for the cab. First find the last known location of the
			// cab before trip start.

			List<Event> cabEvents = cab.getDutyWiseEvents().get(duty);

			Event lastKnownEvent = null;
			Event nextKnownEvent = null;

			if (cabEvents == null) {

			} else {
				Map<String, Event> events = fetchLastAndNextCabEvents(cabEvents, lastKnownEvent, nextKnownEvent, route);
				lastKnownEvent = events.get("last");
				nextKnownEvent = events.get("next");
			}
			possibleCabs = checkFeasibility(lastKnownEvent, nextKnownEvent, route, cab, duty, possibleCabs);
		}
		return possibleCabs;
	}

	private static FeasibilityObject checkFeasibilityOfFirstTripOfDuty(ActualCabPreferences cab, Route route,
			Map<ActualCabPreferences, Double> possibleCabs, Event nextKnownEvent, Event lastKnownEvent, boolean isNextB2B,
			int duty) {
		// Its the first duty for the cab. Will start at the start garage. Can be
		// allocated only if
		// 1. Cab can reach the start location from home starting after their preferred
		// start time (-buffer.. so,
		// for example, a cab's preferred start time is 6:00 and buffer is 30 mins,
		// means a cab can do trip which it needs to start for after 5:30)
		// 2. The start location is an Office
		// OR on way to the office, cab does not need to deviate significantly (say,
		// garage to pickup + 1st pickup to office - garage to office <= 7 kms)
		// OR 1st pickup in within 5 kms of the start garage

		FeasibilityObject feasibility = new FeasibilityObject();
		boolean isLastTrip = false;
		boolean isFirstTrip = true;
		feasibility.setFirstTrip(isFirstTrip);

		if (prevIterAlloc.containsKey(route) && cab.equals(prevIterAlloc.get(route))) {
			// System.out.println("test");
		}

		Geocode startLocation = cab.getStartLoc();
		boolean useLastKnownLoc = checkIfLastKnownLocUsed(cab, route, startLocation);

		double speed = getSpeed(route.getStartTime());
		double emptyLeg = Utils.getAerialDist(startLocation, route.firstEmp().getPickLoc());
		double distOfTripStartPtFromOffice = Utils.getAerialDist(route.getStartLoc(), route.getOffice());
		double garageDistFromOffice = Utils.getAerialDist(startLocation, route.getOffice());

		double angleGSO = 0;
		double angleSGO = 0;

		if (route.getDirection().equalsIgnoreCase("Login")) {
			angleGSO = Utils.getAngle(startLocation, route.getStartLoc(), route.getOffice());
			angleSGO = Utils.getAngle(route.getStartLoc(), startLocation, route.getOffice());
		} else {
			angleGSO = 240;
			angleSGO = 120;
		}

		if (emptyLeg > 15) {
			String reason = "Empty Leg more than 15 kms (" + startLocation + " to " + route.getStartLoc() + ")";

			createFeasibilityOfFirstTripObject(false, reason, useLastKnownLoc, emptyLeg, nextKnownEvent, route, cab,
					isLastTrip, feasibility);
			return feasibility;
		}

		if (emptyLeg <= 5) {
			speed = speed - 2;
		}

		long travelTime = (long) (route.firstEmp().getPickTime().getTime() - (emptyLeg / speed) * (hours) - bufferTime);

		Date homeLeaveTime = new Date(travelTime);

		Date cabDesiredStartTime = cab.getCabDutyStartTimeForRoute(route.getStartTime());
		Date cabDesiredEndTime = cab.getCabDutyEndTimeForRoute(route.getStartTime());

		if (useLastKnownLoc) {
			cabDesiredStartTime = cab.getLastKnownTimestamp();

			if (homeLeaveTime.before(cabDesiredStartTime)) {
				String reason = "Leave Time from last known location(" + startLocation + ") before Start Time. ("
						+ sysDateFormat.format(homeLeaveTime) + " < " + sysDateFormat.format(cabDesiredStartTime) + ")";
				createFeasibilityOfFirstTripObject(false, reason, useLastKnownLoc, emptyLeg, nextKnownEvent, route, cab,
						isLastTrip, feasibility);
				return feasibility;
			}
		} else {
			Date cabDesiredStartTimeWithBuffer = new Date(
					cabDesiredStartTime.getTime() - (cab.getStartTimeThreshold() * minutes) - bufferTime);

			if (homeLeaveTime.before(cabDesiredStartTimeWithBuffer)) {
				// not feasible
				String reason = "First trip needs to start before day start. (" + sysDateFormat.format(homeLeaveTime) + " < "
						+ sysDateFormat.format(cabDesiredStartTime) + " [" + startLocation + "->" + route.getStartLoc() + "->"
						+ route.getOffice() + " = " + angleGSO + "])";

				createFeasibilityOfFirstTripObject(false, reason, useLastKnownLoc, emptyLeg, nextKnownEvent, route, cab,
						isLastTrip, feasibility);
				return feasibility;
			}
		}

		int numTripsInDuty = cab.numTripsInDuty(duty);

		if (numTripsInDuty + 1 > cab.getMaxTripsInDay()) {
			String reason = "Cab has done max possible trips in duty. (" + cab.getMaxTripsInDay() + ")";
			createFeasibilityOfFirstTripObject(false, reason, useLastKnownLoc, emptyLeg, nextKnownEvent, route, cab,
					isLastTrip, feasibility);
			return feasibility;
		}

		// since the cab can do this trip, find the score of the cab to trip
		double score = 0;

		double timeDiffFromStartGarage = ((homeLeaveTime.getTime() - cabDesiredStartTime.getTime()) / (1000 * 60 * 60));

		score = (speed / (1 + Math.pow(emptyLeg, 2))) * (10 / (1 + Math.pow(timeDiffFromStartGarage, 4)));

		if (angleSGO < 210 && angleSGO > 150) {
			score = score * 30 / (Math.abs(angleSGO - 180) + 1);
		}

		if (angleGSO < 210 && angleGSO > 150) {
			score = score * 30 / (Math.abs(angleGSO - 180) + 1);
		} else {
			if ((angleGSO < 30 || angleGSO > 330)
					&& (emptyLeg <= 5 || emptyLeg <= 0.5 * Utils.getAerialDist(route.getStartLoc(), route.getEndLoc()))) {
				score = (score * 30 / (Math.abs(180 - Math.abs(angleGSO - 180)) + 1)) - 1;
			} else {
				score = score * 30 / Math.pow((Math.abs(angleGSO - 180) + 1), 2);
			}

		}

		if (nextKnownEvent == null) {
			double angleOEG = 0;
			double angleOGE = 0;

			angleOEG = Utils.getAngle(route.getOffice(), route.getEndLoc(), cab.getEndLoc());
			angleOGE = Utils.getAngle(route.getOffice(), cab.getEndLoc(), route.getEndLoc());

			Geocode startLocationForLastTrip = route.getEndLoc();
			Date startTimeForLastTrip = route.getEndTime();

			double routeDist = Utils.getAerialDist(route.getStartLoc(), route.getEndLoc());
			Date routeEndTime = route.getEndTime();
			double speedForRoute = getSpeed(routeEndTime);
			long routePlannedTravelTime = route.getEndTime().getTime() - route.getStartTime().getTime();

			if (route.getDistance() >= 25) {
				speedForRoute = route.getDistance()
						/ ((15 / speedForRoute) + ((route.getDistance() - 15) / (speedForRoute * 3)));
			}

			double routePredictedTravelTime = (routeDist / speedForRoute) * (hours);

			if (routePredictedTravelTime - routePlannedTravelTime > 0) {
				startTimeForLastTrip = new Date((long) (route.getStartTime().getTime() + routePredictedTravelTime));
				feasibility.setThisRouteEndTime(startTimeForLastTrip);
			}

			double speedForLastLeg = getSpeed(cab.getEndTime());
			double emptyLegForLastLeg = Utils.getAerialDist(route.getEndLoc(), cab.getEndLoc());
			double distOfTripEndPtFromOffice = Utils.getAerialDist(route.getOffice(), route.getEndLoc());
			double endGarageDistFromOffice = Utils.getAerialDist(route.getOffice(), cab.getEndLoc());

			if (emptyLegForLastLeg <= 5) {
				// 2c. 1st pickup in within 5 kms of the start garage
				speedForLastLeg = speedForLastLeg - 2;
			}

			long travelTimeLastLeg = (long) (startTimeForLastTrip.getTime() + (emptyLegForLastLeg / speedForLastLeg) * (hours)
					+ bufferTime);

			Date lastEmptyLegEndTime = new Date(travelTimeLastLeg);
			cabDesiredEndTime = new Date(cabDesiredEndTime.getTime() + (cab.getEndTimeThreshold() * minutes));

			if (lastEmptyLegEndTime.before(cabDesiredEndTime)) {
				// 1. End time is before cab's desired end time

			} else {

				if ((angleOEG < 30 || angleOEG > 330) && emptyLeg <= 10) {

				} else {
					// check angle of office(O) -> trip end (E) -> Garage (G). OEG should be as
					// close to 180 degree as possible
					if (angleOGE > 150 && angleOGE < 210
							&& ((garageDistFromOffice <= 20 && emptyLeg <= 10) || (garageDistFromOffice > 20 && emptyLeg <= 20))) {
						// System.out.println(
						// route.getOffice() + ";" + cab.getEndLoc() + ";" + route.getEndLoc() + ";" + angleOEG);
					} else {

						if ((angleOGE < 30 || angleOGE > 330)) {

						} else {
							// not feasible
							if (route.getDistance() >= 25 && emptyLeg <= 15) {

							} else {
								String reason = "Cab cannot reach end on Time and trip not in desired direction.("
										+ sysDateFormat.format(lastEmptyLegEndTime) + " > " + sysDateFormat.format(cabDesiredEndTime) + " ["
										+ route.getOffice() + "->" + route.getEndLoc() + "->" + cab.getEndLoc() + " = " + angleOEG + "])";
								createFeasibilityOfFirstTripObject(false, reason, useLastKnownLoc, emptyLegForLastLeg, nextKnownEvent,
										route, cab, isLastTrip, feasibility);
								return feasibility;
							}

						}
					}
				}
			}

			double deviationLastLeg = emptyLegForLastLeg + distOfTripEndPtFromOffice - endGarageDistFromOffice;

			double timeDiffFromEndGarage = cabDesiredEndTime.getTime() - lastEmptyLegEndTime.getTime();

			if (timeDiffFromEndGarage >= (2 * hours)) {

			} else {
				// If trip starts within 5 km, its feasible
				timeDiffFromEndGarage = (timeDiffFromEndGarage / (60.0 * minutes));

				score = score * (speedForLastLeg / (1 + Math.pow(emptyLegForLastLeg, 2)))
						* ((10) / (1 + Math.pow(timeDiffFromEndGarage, 2)));

				if (Utils.getAerialDist(route.getEndLoc(), cab.getEndLoc()) > Utils.getAerialDist(route.getStartLoc(),
						cab.getEndLoc()) && Utils.getAerialDist(route.getStartLoc(), cab.getEndLoc()) <= 10) {

				} else {
					if (angleOGE < 210 && angleOGE > 150) {
						score = score * 30 / (Math.abs(angleOGE - 180) + 1);
					}

					if (angleOEG < 210 && angleOEG > 150) {
						score = score * 30 / (Math.abs(angleOEG - 180) + 1);
					} else {
						if ((angleOEG < 30 || angleOEG > 330) && (emptyLeg <= 5 || emptyLeg <= 0.5 * routeDist)) {
							score = (score * 30 / (Math.abs(180 - Math.abs(angleOEG - 180)) + 1)) - 1;
						} else {
							score = score * 30 / Math.pow((Math.abs(angleOEG - 180) + 1), 2);
						}

					}
				}

				isLastTrip = true;
			}
		} else {
			if (nextKnownEvent.getEventStart().getTime() - route.getEndTime().getTime() >= (3 * hours)) {
				double speed_toNext = getSpeed(nextKnownEvent.getEventStart());
				double emptyLeg_toNext = Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart());
				double waitTime = nextKnownEvent.getEventStart().getTime() - route.getEndTime().getTime()
						- ((emptyLeg_toNext / speed_toNext) * hours);

				waitTime = (waitTime / (60.0 * minutes));
				score = score * (speed_toNext / (1 + Math.pow(emptyLeg_toNext, 2))) * (10 / (1 + Math.pow(waitTime, 4)));

				if (isNextB2B) {
					score = score / 100;
				}

			} else {
				double speed_toNext = getSpeed(nextKnownEvent.getEventStart());
				double emptyLeg_toNext = Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart());
				double waitTime = nextKnownEvent.getEventStart().getTime() - route.getEndTime().getTime()
						- ((emptyLeg_toNext / speed_toNext) * hours);

				waitTime = (waitTime / (60.0 * minutes));
				score = score * (speed_toNext / (1 + Math.pow(emptyLeg_toNext, 2))) * (10 / (1 + Math.pow(waitTime, 4)));

				if (isNextB2B) {
					score = score / 100;
				}
			}
		}

		if (route.getDirection().equalsIgnoreCase("Logout")) {
			score = score / 2;
		}

		if (numTripsInDuty >= (4)) {
			score = score * Math.pow((1 - (numTripsInDuty / cab.getMaxTripsInDay())), 2);
		} else {
			// score = score * (1 + (numTripsInDuty / cab.getMaxTripsInDay()));
		}

		if (numTripsInDuty == 0) {
			score = score / 1000;
		}

		if (cab.getCabBusiness() == 1) {
			score = score * 10;
		}

		if (isNextB2B) {
			score = score / 100;
		}

		possibleCabs.put(cab, score);

		createFeasibilityOfFirstTripObject(true, "", useLastKnownLoc, emptyLeg, nextKnownEvent, route, cab, isLastTrip,
				feasibility);
		return feasibility;
	}

	private static boolean checkIfLastKnownLocUsed(ActualCabPreferences cab, Route route, Geocode startLocation) {
		if (cab.getLastKnownLoc() != null) {
			Geocode lastKnownLocation = cab.getLastKnownLoc();
			Date lastKnownTimestamp = cab.getLastKnownTimestamp();

			if (route.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)) {
				startLocation = lastKnownLocation;
				return true;
			}
		}
		return false;
	}

	private static void createFeasibilityOfFirstTripObject(boolean feasibile, String reason, boolean useLastKnownLoc,
			double emptyLeg, Event nextKnownEvent, Route route, ActualCabPreferences cab, boolean isLastTrip,
			FeasibilityObject feasibility) {
		feasibility.setFeasible(feasibile);
		feasibility.setReason(reason);

		feasibility.setUseLastKnownLocation(useLastKnownLoc);
		feasibility.setDistFromPrev(emptyLeg);
		if (nextKnownEvent == null) {
			if (isLastTrip) {
				double emptyLeg_toGarage = Utils.getAerialDist(route.getEndLoc(), cab.getEndLoc());
				feasibility.setDistToNext(emptyLeg_toGarage);
			}
		} else {
			double emptyLeg_toNext = Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart());
			feasibility.setDistToNext(emptyLeg_toNext);
		}

		feasibility.setLastTrip(isLastTrip);
	}

	private static Map<ActualCabPreferences, Double> checkFeasibility(Event lastKnownEvent, Event nextKnownEvent,
			Route route, ActualCabPreferences cab, int duty, Map<ActualCabPreferences, Double> possibleCabs) {
		if (lastKnownEvent == null) {
			// this is cab's first trip of the duty
			possibleCabs = checkFirstTripFeasibility(lastKnownEvent, nextKnownEvent, route, cab, duty, possibleCabs);
			return possibleCabs;

		} else {
			// this is not cab's first trip of the duty
			possibleCabs = checkNonFirstTripFeasibility(lastKnownEvent, nextKnownEvent, route, cab, duty, possibleCabs);
			return possibleCabs;

		}
	}

	private static Map<ActualCabPreferences, Double> checkFirstTripFeasibility(Event lastKnownEvent, Event nextKnownEvent,
			Route route, ActualCabPreferences cab, int duty, Map<ActualCabPreferences, Double> possibleCabs) {
		if (nextKnownEvent == null) {
			// something is wrong. Both last known and next known cannot be null if cab has
			// a duty
			if (prevIterAlloc != null && prevIterAlloc.get(route) == cab) {
				// System.out.println("Not feasible");
			}
			// writerCabLogs.println(cab.getCabRegnNum() + ";" + route.getRouteID() + ";" + "FALSE"
			// + ";" + 0 + ";" + "something wrong. Cab has a duty but no trips in duty." + ";"
			// + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";"
			// + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA");
			return possibleCabs;

		} else {
			boolean isNextB2B = false;

			if (nextKnownEvent.getEventStart().getTime() - route.getStartTime().getTime() <= (hours)) {
				// back-to-back
				isNextB2B = true;
			}

			Date routeEndTime = predictRouteEndTime(route);

			FeasibilityObject feasibilityOfNextTripForCab = checkFeasibilityOfTrip(route.getEndLoc(),
					nextKnownEvent.getStart(), routeEndTime, nextKnownEvent.getEventStart(), isNextB2B);

			if (feasibilityOfNextTripForCab.isFeasible()) {
				FeasibilityObject feasibilityOfFirstTrip = checkFeasibilityOfFirstTripOfDuty(cab, route, possibleCabs,
						nextKnownEvent, lastKnownEvent, isNextB2B, duty);
				if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfFirstTrip.isFeasible()) {
					// System.out.println("Not feasible");
				}
				// if (numIter == 0 || cab.getCabRegnNum().startsWith("New-" + numIter + "-")) {
				logFeasibilityOfTrip(feasibilityOfFirstTrip, cab, route, possibleCabs, nextKnownEvent, lastKnownEvent,
						"Trip Not Feasible as the First Trip for Cab");
				// }
				return possibleCabs;
			} else {
				if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfNextTripForCab.isFeasible()) {
					// System.out.println("Not feasible");
				}
				// if (numIter == 0 || cab.getCabRegnNum().startsWith("New-" + numIter + "-")) {
				logFeasibilityOfTrip(feasibilityOfNextTripForCab, cab, route, possibleCabs, nextKnownEvent, lastKnownEvent,
						"Trip Not Feasible as the Next Trip for Cab");
				// not feasible
				// }
				return possibleCabs;
			}
		}
	}

	private static Date predictRouteEndTime(Route route) {
		double routeDist = Utils.getAerialDist(route.getStartLoc(), route.getEndLoc());
		Date routeEndTime = route.getEndTime();
		double speedForRoute = getSpeed(routeEndTime);
		long routePlannedTravelTime = route.getEndTime().getTime() - route.getStartTime().getTime();

		if (route.getDistance() >= 25) {
			speedForRoute = route.getDistance() / ((15 / speedForRoute) + ((route.getDistance() - 15) / (speedForRoute * 3)));
		}

		double routePredictedTravelTime = (routeDist / speedForRoute) * (hours);

		if (routePredictedTravelTime - routePlannedTravelTime > 0) {
			routeEndTime = new Date((long) (route.getStartTime().getTime() + routePredictedTravelTime));
		}
		return routeEndTime;
	}

	private static FeasibilityObject checkFeasibilityOfTrip(Geocode start, Geocode end, Date startDate, Date endDate,
			boolean isLastB2B) {
		double speed = getSpeed(endDate);
		double emptyLeg = Utils.getAerialDist(start, end);
		FeasibilityObject feasibility = new FeasibilityObject();

		if (emptyLeg <= 5) {
			speed = speed - 2;
		}

		if (isLastB2B) {
			speed = speed - 2;
		}

		long travelTime = (long) (endDate.getTime() - ((emptyLeg / speed) * (hours)) - bufferTime);

		Date leaveTime = new Date(travelTime);

		/*
		 * if(leaveTime.getTime() - startDate.getTime() < 0) { if(emptyLeg <= 7) { return true; } else { return false; } }
		 */

		if (leaveTime.after(startDate)) {
			feasibility.setFeasible(true);
			feasibility.setReason("");
		} else {
			feasibility.setFeasible(false);
			feasibility.setReason("Cab needs to leave before it can start.(" + sysDateFormat.format(leaveTime) + " < "
					+ sysDateFormat.format(startDate) + ")");
		}

		return feasibility;
	}

	private static void logFeasibilityOfTrip(FeasibilityObject feasibilityOfTrip, ActualCabPreferences cab, Route route,
			Map<ActualCabPreferences, Double> possibleCabs, Event nextKnownEvent, Event lastKnownEvent, String reason) {
		String lastLocation = "NA";
		String lastLocationType = "NA";

		if (feasibilityOfTrip.isUseLastKnownLocation()) {
			lastLocation = cab.getLastKnownLoc().toString();
			lastLocationType = "Last Known Loc";
		} else {
			if (feasibilityOfTrip.isFirstTrip()) {
				lastLocation = cab.getStartLoc().toString();
				lastLocationType = "Start Garage";
			} else {
				if (lastKnownEvent == null) {

				} else {
					lastLocation = lastKnownEvent.getEnd().toString();
					lastLocationType = "Trip End(" + lastKnownEvent.getEventId() + ")";
				}
			}
		}

		String nextLoc = "NA";
		String nextLocType = "NA";
		if (feasibilityOfTrip.isLastTrip()) {
			nextLoc = cab.getEndLoc().toString();
			nextLocType = "End Garage";
		} else {
			if (nextKnownEvent == null) {

			} else {
				nextLoc = nextKnownEvent.getStart().toString();
				nextLocType = "Trip Start (" + nextKnownEvent.getEventId() + ")";
			}
		}

		String prevRouteEndTime = "";
		if (feasibilityOfTrip.getPrevRouteEndTime() != null) {
			prevRouteEndTime = sysDateFormat.format(feasibilityOfTrip.getPrevRouteEndTime());
		}

		String thisRouteEndTime = "";
		if (feasibilityOfTrip.getThisRouteEndTime() != null) {
			thisRouteEndTime = sysDateFormat.format(feasibilityOfTrip.getThisRouteEndTime());
		}

		// if (feasibilityOfTrip.isFeasible()) {
		// writerCabLogs.println(cab.getCabRegnNum() + ";" + route.getRouteID() + ";" + "TRUE" + ";"
		// + possibleCabs.get(cab) + ";" + "" + ";" + feasibilityOfTrip.getReason() + ";" + lastLocationType
		// + ";" + lastLocation + ";" + route.getStartLoc() + ";" + route.getEndLoc() + ";" + nextLoc + ";"
		// + nextLocType + ";" + feasibilityOfTrip.isFirstTrip() + ";" + feasibilityOfTrip.isLastTrip() + ";"
		// + feasibilityOfTrip.getDistFromPrev() + ";" + feasibilityOfTrip.getDistToNext() + ";"
		// + prevRouteEndTime + ";" + thisRouteEndTime);
		// return;
		//
		// } else {
		// writerCabLogs.println(cab.getCabRegnNum() + ";" + route.getRouteID() + ";" + "FALSE" + ";" + 0 + ";"
		// + reason + ";" + feasibilityOfTrip.getReason() + ";" + lastLocationType + ";" + lastLocation + ";"
		// + route.getStartLoc() + ";" + route.getEndLoc() + ";" + nextLoc + ";" + nextLocType + ";" + true
		// + ";" + feasibilityOfTrip.isLastTrip() + ";" + feasibilityOfTrip.getDistFromPrev() + ";" + "NA"
		// + ";" + prevRouteEndTime + ";" + thisRouteEndTime);
		// return;
		// }

	}

	private static Map<ActualCabPreferences, Double> checkNonFirstTripFeasibility(Event lastKnownEvent,
			Event nextKnownEvent, Route route, ActualCabPreferences cab, int duty,
			Map<ActualCabPreferences, Double> possibleCabs) {

		Geocode startLoc = lastKnownEvent.getEnd();
		Date startTime = predictLastKnownEventEnd(lastKnownEvent);

		boolean useLastKnownLoc = false;
		if (cab.getLastKnownLoc() != null && cab.getLastKnownTimestamp().after(startTime)) {
			startLoc = cab.getLastKnownLoc();
			startTime = cab.getLastKnownTimestamp();
			useLastKnownLoc = true;
		}

		boolean isLastB2B = false;
		boolean isNextB2B = false;

		if (route.getStartTime().getTime() - lastKnownEvent.getEventStart().getTime() <= (hours)) {
			isLastB2B = true;
		}

		if (nextKnownEvent != null
				&& nextKnownEvent.getEventStart().getTime() - route.getStartTime().getTime() <= (hours)) {
			isNextB2B = true;
		}

		if (nextKnownEvent == null) {
			FeasibilityObject feasibilityOfTrip = checkFeasibilityOfTrip(startLoc, route.getStartLoc(), startTime,
					route.getStartTime(), isLastB2B);
			if (feasibilityOfTrip.isFeasible()) {
				FeasibilityObject feasibilityOfLastTrip = checkFeasibilityOfLastTripOfDuty(cab, route, possibleCabs,
						lastKnownEvent, isLastB2B);
				if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfLastTrip.isFeasible()) {
					// System.out.println("Not feasible");
				}
				logFeasibilityOfTrip(feasibilityOfLastTrip, cab, route, possibleCabs, nextKnownEvent, lastKnownEvent,
						"Trip Not Feasible as the Last Trip for Cab");
				return possibleCabs;
			} else {
				// unsuccessful
				if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfTrip.isFeasible()) {
					// System.out.println("Not feasible");
				}
				logFeasibilityOfTrip(feasibilityOfTrip, cab, route, possibleCabs, nextKnownEvent, lastKnownEvent,
						"Trip Not Feasible as the Next Trip for Cab");
				return possibleCabs;
			}
		} else {

			FeasibilityObject feasibilityOfMidTrip = checkFeasibilityOfMidTripOfDuty(cab, route, possibleCabs, lastKnownEvent,
					nextKnownEvent, isLastB2B, isNextB2B);
			if (prevIterAlloc != null && prevIterAlloc.get(route) == cab && !feasibilityOfMidTrip.isFeasible()) {
				// System.out.println("Not feasible");
			}
			logFeasibilityOfTrip(feasibilityOfMidTrip, cab, route, possibleCabs, nextKnownEvent, lastKnownEvent,
					"Trip Not Feasible as the Mid Trip for Cab");

			return possibleCabs;
		}
	}

	private static FeasibilityObject checkFeasibilityOfLastTripOfDuty(ActualCabPreferences cab, Route route,
			Map<ActualCabPreferences, Double> possibleCabs, Event lastKnownEvent, boolean isLastB2B) {
		// Its the last duty for the cab. Will end at the end garage. Can be
		// allocated only if
		// 1. Cab can reach the end garage from trip end before their preferred
		// end time
		// 2. Either there is at-least 3 hours before day end for the cab
		// OR on way to the garage from trip's office to last drop to garage, cab does
		// not need to deviate significantly (say,
		// office to last drop + last drop from office - garage to office <= 7 kms)
		// OR last drop in within 5 kms of the end garage
		FeasibilityObject feasibility = new FeasibilityObject();

		Geocode startLocation = lastKnownEvent.getEnd();
		Date startTime = lastKnownEvent.getEventEnd();

		double lastKnownRouteDist = Utils.getAerialDist(lastKnownEvent.getStart(), lastKnownEvent.getEnd());
		Date lastKnownRouteEndTime = lastKnownEvent.getEventEnd();
		double speedForLastKnownRoute = getSpeed(lastKnownRouteEndTime);
		long lastKnownRoutePlannedTravelTime = lastKnownEvent.getEventEnd().getTime()
				- lastKnownEvent.getEventStart().getTime();

		if (lastKnownEvent.getDistance() >= 25) {
			speedForLastKnownRoute = lastKnownEvent.getDistance()
					/ ((15 / speedForLastKnownRoute) + ((lastKnownEvent.getDistance() - 15) / (speedForLastKnownRoute * 3)));
		}

		double lastKnownRoutePredictedTravelTime = (lastKnownRouteDist / speedForLastKnownRoute) * (hours);

		if (lastKnownRoutePredictedTravelTime - lastKnownRoutePlannedTravelTime > 0) {
			startTime = new Date((long) (lastKnownEvent.getEventStart().getTime() + lastKnownRoutePredictedTravelTime));
			feasibility.setPrevRouteEndTime(
					new Date((long) (lastKnownEvent.getEventStart().getTime() + lastKnownRoutePredictedTravelTime)));
		}

		boolean useLastKnownLoc = false;
		if (cab.getLastKnownLoc() != null && cab.getLastKnownTimestamp().after(startTime)) {
			startLocation = cab.getLastKnownLoc();
			startTime = cab.getLastKnownTimestamp();
			useLastKnownLoc = true;
		}

		double distStartLocationToEndGarage = Utils.getAerialDist(startLocation, cab.getEndLoc());

		FeasibilityObject feasibilityOfTrip = checkFeasibilityOfTrip(startLocation, route.getStartLoc(), startTime,
				route.getStartTime(), isLastB2B);
		if (feasibilityOfTrip.isFeasible()) {
			// trip feasible from last trip
		} else {
			feasibility.setFeasible(false);
			feasibility.setReason(feasibilityOfTrip.getReason());
			return feasibility;
		}

		double angleOEG = 0;
		double angleOGE = 0;

		if (route.getDirection().equalsIgnoreCase("Logout")) {
			angleOEG = Utils.getAngle(route.getOffice(), route.getEndLoc(), cab.getEndLoc());
			angleOGE = Utils.getAngle(route.getOffice(), cab.getEndLoc(), route.getEndLoc());
		} else {
			angleOEG = 240;
			angleOGE = 360;
		}

		double speedToGarage = getSpeed(cab.getEndTime());
		double emptyLegToGarage = Utils.getAerialDist(route.getEndLoc(), cab.getEndLoc());
		double distOfTripEndPtFromOffice = Utils.getAerialDist(route.getOffice(), route.getEndLoc());
		double garageDistFromOffice = Utils.getAerialDist(route.getOffice(), cab.getEndLoc());

		Date routeEndTime = route.getEndTime();

		double routeDist = Utils.getAerialDist(route.getStartLoc(), route.getEndLoc());
		double speedForRoute = getSpeed(routeEndTime);
		long routePlannedTravelTime = route.getEndTime().getTime() - route.getStartTime().getTime();

		if (route.getDistance() >= 25) {
			speedForRoute = route.getDistance() / ((15 / speedForRoute) + ((route.getDistance() - 15) / (speedForRoute * 3)));
		}

		double routePredictedTravelTime = (routeDist / speedForRoute) * (hours);

		if (routePredictedTravelTime - routePlannedTravelTime > 0) {
			routeEndTime = new Date((long) (route.getStartTime().getTime() + routePredictedTravelTime));
			feasibility.setPrevRouteEndTime(routeEndTime);
		}

		long travelTime = (long) (routeEndTime.getTime() + (emptyLegToGarage / speedToGarage) * (hours) + bufferTime);

		Date lastEmptyLegEndTime = new Date(travelTime);

		Date cabDesiredEndTime;// = cab.getStartTime();

		cabDesiredEndTime = new Date(lastEmptyLegEndTime.getTime());
		cabDesiredEndTime.setHours(cab.getEndTime().getHours());
		cabDesiredEndTime.setMinutes(cab.getEndTime().getMinutes());
		cabDesiredEndTime.setSeconds(0);

		long timeDiffFromEndGarage = cabDesiredEndTime.getTime() - lastEmptyLegEndTime.getTime();

		if (timeDiffFromEndGarage > (12 * hours)) {
			timeDiffFromEndGarage = timeDiffFromEndGarage - (24 * hours);
		}

		if (timeDiffFromEndGarage < (-12 * hours)) {
			timeDiffFromEndGarage = timeDiffFromEndGarage + (24 * hours);
		}

		if (timeDiffFromEndGarage >= -1 * cab.getEndTimeThreshold() * minutes) {
			// 1. End time is before cab's desired end time

		} else {

			// check angle of office(O) -> trip end (E) -> Garage (G). OEG should be as
			// close to 180 degree as possible
			if (angleOEG > 150 && angleOEG < 210) {

			} else {

				if ((angleOEG < 30 || angleOEG > 330) && emptyLegToGarage <= 10) {

				} else {
					// check angle of office(O) -> trip end (E) -> Garage (G). OEG should be as
					// close to 180 degree as possible
					if (angleOGE > 150 && angleOGE < 210 && ((garageDistFromOffice <= 20 && emptyLegToGarage <= 10)
							|| (garageDistFromOffice > 20 && emptyLegToGarage <= 20))) {

					} else {

						if ((angleOGE < 30 || angleOGE > 330)) {

						} else {
							// not feasible
							if (route.getDistance() >= 25 && emptyLegToGarage <= 15) {

							} else {
								feasibility.setFeasible(false);
								feasibility.setReason("Cab cannot reach end on Time and trip not in desired direction.("
										+ sysDateFormat.format(lastEmptyLegEndTime) + " > " + sysDateFormat.format(cabDesiredEndTime) + " ["
										+ route.getOffice() + "->" + route.getEndLoc() + "->" + cab.getEndLoc() + " = " + angleOEG + "])");
								feasibility.setLastTrip(true);
								feasibility.setFirstTrip(false);
								feasibility.setUseLastKnownLocation(useLastKnownLoc);
								feasibility.setDistFromPrev(Utils.getAerialDist(startLocation, route.getStartLoc()));
								feasibility.setDistToNext(emptyLegToGarage);
								return feasibility;
							}

						}
					}
				}

			}

		}

		int numTripsInDuty = cab.numTripsInDuty(cab.getCurrentDuty());

		if (numTripsInDuty + 1 > cab.getMaxTripsInDay()) {
			feasibility.setFeasible(false);
			feasibility.setReason("Cab has done max possible trips in duty. (" + cab.getMaxTripsInDay() + ")");
			feasibility.setLastTrip(true);
			feasibility.setFirstTrip(false);
			feasibility.setUseLastKnownLocation(useLastKnownLoc);
			feasibility.setDistFromPrev(Utils.getAerialDist(startLocation, route.getStartLoc()));
			feasibility.setDistToNext(emptyLegToGarage);
			return feasibility;

		}

		if (timeDiffFromEndGarage >= (2 * hours)) {
			double score = 0;

			double speed_fromLast = getSpeed(route.getStartTime());
			double emptyLeg_fromLast = Utils.getAerialDist(startLocation, route.getStartLoc());
			double waitTime = route.getStartTime().getTime() - startTime.getTime()
					- ((emptyLeg_fromLast / speed_fromLast) * (hours));

			waitTime = (waitTime / (60.0 * minutes));

			score = ((speed_fromLast) / (1 + Math.pow(emptyLeg_fromLast, 2))) * ((10) / (1 + Math.pow(waitTime, 4)));

			if (numTripsInDuty >= (4)) {
				score = score * Math.pow((1 - (numTripsInDuty / cab.getMaxTripsInDay())), 2);
			} else {
				// score = score * (1 + (numTripsInDuty / cab.getMaxTripsInDay()));
			}

			if (cab.getCabBusiness() == 1) {
				score = score * 10;
			}

			if (cab.getCurrentDuty() == 0) {
				score = score / 10;
			}

			if (route.getDistance() >= 15) {
				if (emptyLegToGarage <= 8) {
					score = score * 0.5;
				}
			} else {
				if (emptyLegToGarage <= 5) {
					score = score * 0.5;
				}
			}

			if (isLastB2B) {
				score = score / 100;
			}
			possibleCabs.put(cab, score);

			feasibility.setLastTrip(false);
			feasibility.setFirstTrip(false);
			feasibility.setUseLastKnownLocation(useLastKnownLoc);
			feasibility.setDistFromPrev(Utils.getAerialDist(startLocation, route.getStartLoc()));
			feasibility.setDistToNext(emptyLegToGarage);

			feasibility.setFeasible(true);
			feasibility.setReason("");
			return feasibility;

		} else {
			/*
			 * if(route.getDistance() <= 30) { // If trip starts within 5 km, its feasible if (emptyLeg <= 5) { // 2c. 1st
			 * pickup in within 5 kms of the start garage } else { // 2a. and 2b. The start location is an Office OR // on way
			 * to the office, cab does not need to deviate significantly (say, garage // to pickup + 1st pickup to office -
			 * garage to office <= 7 kms) if (emptyLeg + distOfTripEndPtFromOffice - garageDistFromOffice <= 7) { } else { //
			 * not feasible return false; } } } else { // If trip starts within 5 km, its feasible if (emptyLeg <= 7) { // 2c.
			 * 1st pickup in within 5 kms of the start garage } else { // 2a. and 2b. The start location is an Office OR // on
			 * way to the office, cab does not need to deviate significantly (say, garage // to pickup + 1st pickup to office
			 * - garage to office <= 7 kms) if (emptyLeg + distOfTripEndPtFromOffice - garageDistFromOffice <= 9) { } else {
			 * // not feasible return false; } } }
			 */

			if (emptyLegToGarage <= 5 || route.getDistance() <= 7) {

			} else {
				if (angleOEG > 150 && angleOEG < 210) {
				} else {
					if ((angleOEG < 30 || angleOEG > 330) && emptyLegToGarage <= 10) {
					} else {
						// check angle of office(O) -> trip end (E) -> Garage (G). OEG should be as
						// close to 180 degree as possible
						angleOGE = Utils.getAngle(route.getOffice(), cab.getEndLoc(), route.getEndLoc());
						if (angleOGE > 150 && angleOGE < 210 && emptyLegToGarage <= 7) {
						} else {
							/*
							 * if(route.getDistance() <= 30) { if (emptyLeg + distOfTripEndPtFromOffice - garageDistFromOffice <= 7) {
							 * 
							 * } else { // not feasible return false; } } else { // If trip starts within 5 km, its feasible if
							 * (emptyLeg <= 7) { // 2c. 1st pickup in within 5 kms of the start garage } else { // 2a. and 2b. The
							 * start location is an Office OR // on way to the office, cab does not need to deviate significantly
							 * (say, garage // to pickup + 1st pickup to office - garage to office <= 7 kms) if (emptyLeg +
							 * distOfTripEndPtFromOffice - garageDistFromOffice <= 9) { } else { // not feasible return false; } } }
							 */
						}

					}
				}
			}

		}

		// since the cab can do this trip, find the score of the cab to trip
		// score is defined as a factor of -
		// travel time to pickup point
		// distance to pickup point
		// deviation from office path
		// difference from start time
		// direction of trip
		// # of trips already done
		// hours cab has already run

		double score = 0;

		double speed_fromLast = getSpeed(route.getStartTime());
		double emptyLeg_fromLast = Utils.getAerialDist(startLocation, route.getStartLoc());
		double waitTime = route.getStartTime().getTime() - startTime.getTime()
				- ((emptyLeg_fromLast / speed_fromLast) * (hours));

		waitTime = (waitTime / (60.0 * minutes));
		double timeDiffFromEndGarageDbl = timeDiffFromEndGarage / (60.0 * minutes);

		score = ((speed_fromLast) / (1 + Math.pow(emptyLeg_fromLast, 2))) * ((10) / (1 + Math.pow(waitTime, 4)));

		score = score * (speedToGarage / (Math.pow(emptyLegToGarage, 2) + 1))
				* (10 / (1 + Math.pow(timeDiffFromEndGarageDbl, 2)));

		if (numTripsInDuty >= (4)) {
			score = score * Math.pow((1 - (numTripsInDuty / cab.getMaxTripsInDay())), 2);
		} else {
			// score = score * (1 + (numTripsInDuty / cab.getMaxTripsInDay()));
		}

		if (cab.getCabBusiness() == 1) {
			score = score * 10;
		}

		if (numTripsInDuty == 0) {
			score = score / 10;
		}

		if (route.getDirection().equalsIgnoreCase("Login")) {
			score = score / 2;
		}

		if (angleOGE < 210 && angleOGE > 150) {
			score = score * 30 / (Math.abs(angleOGE - 180) + 1);
		}

		if (angleOEG < 210 && angleOEG > 150) {
			score = score * 30 / (Math.abs(angleOEG - 180) + 1);
		} else {
			if ((angleOEG < 30 || angleOEG > 330) && (emptyLegToGarage <= 5 || emptyLegToGarage <= 0.5 * routeDist)) {
				score = (score * 30 / (Math.abs(180 - Math.abs(angleOEG - 180)) + 1)) - 1;
			} else {
				score = score * 30 / Math.pow((Math.abs(angleOEG - 180) + 1), 2);
			}

		}

		/*
		 * if(numTripsInDuty == 0) { score = 0.5 * score; }
		 */

		if (distStartLocationToEndGarage <= 5 && garageDistFromOffice >= 15) {
			score = score / (garageDistFromOffice / distStartLocationToEndGarage);
		}

		if (isLastB2B) {
			score = score / 100;
		}

		possibleCabs.put(cab, score);

		feasibility.setFeasible(true);
		feasibility.setReason("");
		feasibility.setLastTrip(true);
		feasibility.setFirstTrip(false);
		feasibility.setUseLastKnownLocation(useLastKnownLoc);
		feasibility.setDistFromPrev(Utils.getAerialDist(startLocation, route.getStartLoc()));
		feasibility.setDistToNext(emptyLegToGarage);

		return feasibility;

	}

	private static FeasibilityObject checkFeasibilityOfMidTripOfDuty(ActualCabPreferences cab, Route route,
			Map<ActualCabPreferences, Double> possibleCabs, Event lastKnownEvent, Event nextKnownEvent, boolean isLastB2B,
			boolean isNextB2B) {
		// Its the first duty for the cab. Will start at the start garage. Can be
		// allocated only if
		// 1. Cab can reach the start location from home starting after their preferred
		// start time (-buffer.. so,
		// for example, a cab's preferred start time is 6:00 and buffer is 30 mins,
		// means a cab can do trip which it needs to start for after 5:30)
		// 2. The start location is an Office
		// OR on way to the office, cab does not need to deviate significantly (say,
		// garage to pickup + 1st pickup to office - garage to office <= 7 kms)
		// OR 1st pickup in within 5 kms of the start garage
		FeasibilityObject feasibility = new FeasibilityObject();
		feasibility.setFirstTrip(false);
		feasibility.setLastTrip(false);
		int numTripsInDuty = cab.numTripsInDuty(cab.getCurrentDuty());

		boolean useLastKnownLoc = false;

		if (numTripsInDuty + 1 > cab.getMaxTripsInDay()) {
			feasibility.setFeasible(false);
			feasibility.setReason("Cab has done max possible trips in duty. (" + cab.getMaxTripsInDay() + ")");
			feasibility.setUseLastKnownLocation(false);
			return feasibility;
		}

		Geocode startLocation = lastKnownEvent.getEnd();
		Date desiredStartTime = lastKnownEvent.getEventEnd();

		double lastKnownRouteDist = Utils.getAerialDist(lastKnownEvent.getStart(), lastKnownEvent.getEnd());
		Date lastKnownRouteEndTime = lastKnownEvent.getEventEnd();
		double speedForLastKnownRoute = getSpeed(lastKnownRouteEndTime);
		long lastKnownRoutePlannedTravelTime = lastKnownEvent.getEventEnd().getTime()
				- lastKnownEvent.getEventStart().getTime();

		if (lastKnownEvent.getDistance() >= 25) {
			speedForLastKnownRoute = lastKnownEvent.getDistance()
					/ ((15 / speedForLastKnownRoute) + ((lastKnownEvent.getDistance() - 15) / (speedForLastKnownRoute * 3)));
		}

		double lastKnownRoutePredictedTravelTime = (lastKnownRouteDist / speedForLastKnownRoute) * (hours);

		if (lastKnownRoutePredictedTravelTime - lastKnownRoutePlannedTravelTime > 0) {
			desiredStartTime = new Date(
					(long) (lastKnownEvent.getEventStart().getTime() + lastKnownRoutePredictedTravelTime));
			feasibility.setPrevRouteEndTime(
					new Date((long) (lastKnownEvent.getEventStart().getTime() + lastKnownRoutePredictedTravelTime)));
		}

		if (cab.getLastKnownLoc() != null) {
			Geocode lastKnownLocation = cab.getLastKnownLoc();
			Date lastKnownTimestamp = cab.getLastKnownTimestamp();

			if (route.getStartTime().getTime() - lastKnownTimestamp.getTime() <= (3 * hours)
					&& lastKnownTimestamp.after(desiredStartTime)) {
				startLocation = lastKnownLocation;
				desiredStartTime = lastKnownTimestamp;
				useLastKnownLoc = true;
			}
		}

		double speed = getSpeed(route.getStartTime());
		double emptyLeg = Utils.getAerialDist(startLocation, route.firstEmp().getPickLoc());

		if (emptyLeg <= 5) {
			speed = speed - 2;
		}

		if (isLastB2B) {
			speed = speed - 2;
		}

		long travelTime = (long) (route.firstEmp().getPickTime().getTime() - (emptyLeg / speed) * (hours) - (bufferTime));

		Date startTimeFromLastTripEnd = new Date(travelTime);

		if (startTimeFromLastTripEnd.before(desiredStartTime)) {
			feasibility.setFeasible(false);
			feasibility.setUseLastKnownLocation(useLastKnownLoc);
			feasibility.setDistFromPrev(emptyLeg);
			feasibility.setDistToNext(Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart()));
			feasibility.setReason("Cab needs to start from last known location before desired.("
					+ sysDateFormat.format(startTimeFromLastTripEnd) + " < " + sysDateFormat.format(desiredStartTime) + ")");

			return feasibility;
		}

		double waitTime = route.getStartTime().getTime() - desiredStartTime.getTime() - ((emptyLeg / speed) * hours);

		double routeDist = Utils.getAerialDist(route.getStartLoc(), route.getEndLoc());
		Date routeEndTime = route.getEndTime();
		double speedForRoute = getSpeed(routeEndTime);
		long routePlannedTravelTime = route.getEndTime().getTime() - route.getStartTime().getTime();

		if (route.getDistance() >= 25) {
			speedForRoute = route.getDistance() / ((15 / speedForRoute) + ((route.getDistance() - 15) / (speedForRoute * 3)));
		}

		double routePredictedTravelTime = (routeDist / speedForRoute) * (hours);

		if (routePredictedTravelTime - routePlannedTravelTime > 0) {
			routeEndTime = new Date((long) (route.getStartTime().getTime() + routePredictedTravelTime));
			feasibility.setThisRouteEndTime(routeEndTime);
		}

		FeasibilityObject feasibilityOfTrip = checkFeasibilityOfTrip(route.getEndLoc(), nextKnownEvent.getStart(),
				routeEndTime, nextKnownEvent.getEventStart(), isNextB2B);
		if (feasibilityOfTrip.isFeasible()) {

		} else {
			feasibilityOfTrip.setUseLastKnownLocation(useLastKnownLoc);
			feasibilityOfTrip.setDistFromPrev(emptyLeg);
			feasibilityOfTrip.setDistToNext(Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart()));

			return feasibilityOfTrip;
		}

		// since the cab can do this trip, find the score of the cab to trip
		// score is defined as a factor of -
		// travel time to pickup point
		// distance to pickup point
		// deviation from office path
		// difference from start time
		// direction of trip
		// # of trips already done
		// hours cab has already run

		double score = 0;

		waitTime = (waitTime / (60.0 * minutes));

		score = (speed / (Math.pow(emptyLeg, 2) + 1)) * ((10) / (1 + Math.pow(waitTime, 4)));

		double speed_toNext = getSpeed(nextKnownEvent.getEventStart());
		double emptyLeg_toNext = Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart());
		double waitTimeToNext = nextKnownEvent.getEventStart().getTime() - route.getEndTime().getTime()
				- ((emptyLeg_toNext / speed_toNext) * hours);

		waitTimeToNext = (waitTimeToNext / (60.0 * minutes));

		score = score * (speed_toNext / (1 + Math.pow(emptyLeg_toNext, 2))) * ((10) / (1 + Math.pow(waitTimeToNext, 4)));

		if (numTripsInDuty >= (4)) {
			score = score * Math.pow((1 - (numTripsInDuty / cab.getMaxTripsInDay())), 2);
		} else {
			// score = score * (1 + (numTripsInDuty / cab.getMaxTripsInDay()));
		}

		if (numTripsInDuty == 0) {
			score = score / 100;
		}

		if (cab.getCabBusiness() == 1) {
			score = score * 10;
		}

		if (isLastB2B) {
			score = score / 100;
		}

		if (isNextB2B) {
			score = score / 100;
		}

		possibleCabs.put(cab, score);

		feasibility.setFeasible(true);
		feasibility.setReason("");
		feasibility.setUseLastKnownLocation(useLastKnownLoc);
		feasibility.setDistFromPrev(emptyLeg);
		feasibility.setDistToNext(Utils.getAerialDist(route.getEndLoc(), nextKnownEvent.getStart()));

		return feasibility;
	}

	private static Date predictLastKnownEventEnd(Event lastKnownEvent) {
		Date startTime = lastKnownEvent.getEventEnd();

		double lastKnownRouteDist = Utils.getAerialDist(lastKnownEvent.getStart(), lastKnownEvent.getEnd());
		Date lastKnownRouteEndTime = lastKnownEvent.getEventEnd();
		double speedForLastKnownRoute = getSpeed(lastKnownRouteEndTime);

		if (lastKnownEvent.getDistance() >= 25) {
			speedForLastKnownRoute = lastKnownEvent.getDistance()
					/ ((15 / speedForLastKnownRoute) + ((lastKnownEvent.getDistance() - 15) / (speedForLastKnownRoute * 3)));
		}

		if (lastKnownEvent.getDistance() >= 25) {
			speedForLastKnownRoute = lastKnownEvent.getDistance()
					/ ((15 / speedForLastKnownRoute) + (lastKnownEvent.getDistance() - 15) / (speedForLastKnownRoute * 3));
		}

		long lastKnownRoutePlannedTravelTime = lastKnownEvent.getEventEnd().getTime()
				- lastKnownEvent.getEventStart().getTime();

		double lastKnownRoutePredictedTravelTime = (lastKnownRouteDist / speedForLastKnownRoute) * (hours);

		if (lastKnownRoutePredictedTravelTime - lastKnownRoutePlannedTravelTime > 0) {
			lastKnownRouteEndTime = new Date(
					(long) (lastKnownEvent.getEventStart().getTime() + lastKnownRoutePredictedTravelTime));
			startTime = lastKnownRouteEndTime;
		}
		return startTime;
	}

	private static boolean routeWithinCabTime(Route route, ActualCabPreferences cab) {
		Date cabEndTime = new Date(cab.getEndTime().getTime());
		Date cabStartTime = new Date(cab.getStartTime().getTime() - (cab.getStartTimeThreshold() * minutes));

		Date routeStartTimeForVerify = route.getStartTime();
		Date routeEndTimeForVerify = route.getEndTime();

		Map<String, Date> cabTimes = setCabStartEndTime(cabStartTime, cabEndTime, routeStartTimeForVerify);
		cabEndTime = cabTimes.get("cabEndTime");
		cabStartTime = cabTimes.get("cabStartTime");

		if (routeStartTimeForVerify.before(cabEndTime) && routeStartTimeForVerify.after(cabStartTime)) {

			cabEndTime = new Date(cabEndTime.getTime() + (cab.getEndTimeThreshold() * minutes));
			if (routeEndTimeForVerify.after(cabStartTime) && routeEndTimeForVerify.before(cabEndTime)) {

			} else {
				// not feasible
				if (prevIterAlloc != null && prevIterAlloc.get(route) == cab) {
					// System.out.println("Not feasible");
				}
				// writerCabLogs.println(cab.getCabRegnNum() + ";" + route.getRouteID() + ";" + "FALSE" + ";"
				// + 0 + ";" + "trip starts after cab's preferred end time" + ";" + "("
				// + sysDateFormat.format(routeStartTimeForVerify) + " > "
				// + sysDateFormat.format(cabEndTime) + ")" + ";" + "NA" + ";" + "NA" + ";"
				// + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA"
				// + ";" + "NA" + ";" + "NA" + ";" + "NA");

				return false;
			}

		} else {
			// not feasible
			if (prevIterAlloc != null && prevIterAlloc.get(route) == cab) {
				// System.out.println("Not feasible");
			}
			// writerCabLogs.println(cab.getCabRegnNum() + ";" + route.getRouteID() + ";" + "FALSE" + ";"
			// + 0 + ";" + "trip starts after cab's preferred end time" + ";" + "("
			// + sysDateFormat.format(routeEndTimeForVerify) + " > "
			// + sysDateFormat.format(cabEndTime) + ")" + ";" + "NA" + ";" + "NA" + ";"
			// + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA" + ";" + "NA"
			// + ";" + "NA" + ";" + "NA" + ";" + "NA");

			return false;
		}

		return true;
	}

	private static Map<String, Date> setCabStartEndTime(Date cabStartTime, Date cabEndTime,
			Date routeStartTimeForVerify) {
		cabStartTime.setYear(routeStartTimeForVerify.getYear());
		cabStartTime.setMonth(routeStartTimeForVerify.getMonth());
		cabStartTime.setDate(routeStartTimeForVerify.getDate());

		cabEndTime.setYear(routeStartTimeForVerify.getYear());
		cabEndTime.setMonth(routeStartTimeForVerify.getMonth());
		cabEndTime.setDate(routeStartTimeForVerify.getDate());

		if (cabEndTime.before(cabStartTime)) {
			cabEndTime = new Date(cabEndTime.getTime() + (24 * 60 * 60 * 1000));
		}

		if (routeStartTimeForVerify.after(cabEndTime) && routeStartTimeForVerify.after(cabStartTime)) {
			cabEndTime = new Date(cabEndTime.getTime() + (24 * hours));
			cabStartTime = new Date(cabStartTime.getTime() + (24 * hours));
		} else {
			if (routeStartTimeForVerify.before(cabEndTime) && routeStartTimeForVerify.before(cabStartTime)) {
				cabEndTime = new Date(cabEndTime.getTime() - (24 * hours));
				cabStartTime = new Date(cabStartTime.getTime() - (24 * hours));
			}
		}

		Map<String, Date> cabTimes = new HashMap<String, Date>();
		cabTimes.put("cabStartTime", cabStartTime);
		cabTimes.put("cabEndTime", cabEndTime);

		return cabTimes;

	}

	private static void addNewCabsForRemainingRoutes(Map<Route, Map<ActualCabPreferences, Double>> mapCabRoutes) {

		for (Route route : mapCabRoutes.keySet()) {
			ActualCabPreferences newCab = addNewCab(route);

			allocateCabToRoute(newCab, route, 100);
		}
	}

	private static ActualCabPreferences addNewCab(Route route) {
		ActualCabPreferences actCab = new ActualCabPreferences();

		cabRegnNumStart++;

		String cabRegn = "New-" + numIter + "-" + cabRegnNumStart;
		actCab.setCabRegnNum(cabRegn);

		int cabCap = Integer.parseInt(route.getCabType().split(" ")[0]);

		int startTimeThreshold = 30;
		actCab.setStartTimeThreshold(startTimeThreshold);

		Geocode startGeoCode = route.getStartLoc();
		Date startTime = new Date(route.getStartTime().getTime() + (15 * 60 * 1000));
		int mins = startTime.getMinutes();
		mins = mins - (mins % 15);
		startTime.setMinutes(mins);
		actCab.setStartTime(startTime);

		if (route.getDirection().equalsIgnoreCase("Login")) {
			double garageDistFromStart = route.getDistance();
			startGeoCode = Utils.getMidPoint(route.getStartLoc(), route.getEndLoc(), garageDistFromStart);
			garageLogin++;
			actCab.setStartLoc(startGeoCode);
		} else {
			double garageDistFromStart = route.getDistance();
			startGeoCode = Utils.getMidPoint(route.getStartLoc(), route.getEndLoc(), garageDistFromStart);
			actCab.setStartLoc(startGeoCode);
			garageLogout++;
		}

		Date endTime = new Date(startTime.getTime() + (14 * hours));
		actCab.setEndTime(endTime);

		int endTimeThreshold = 30;
		actCab.setEndTimeThreshold(endTimeThreshold);

		int endTimeExtn = 0;
		actCab.setEndTimeExtn(endTimeExtn);

		Geocode endGeoCode = startGeoCode;
		actCab.setEndLoc(endGeoCode);

		double maxKmInDay = 200;
		actCab.setMaxKmInDay(maxKmInDay);

		int maxTripsInDay = 8;
		actCab.setMaxTripsInDay(maxTripsInDay);

		int get2WorkCab = 0;
		actCab.setCabBusiness(get2WorkCab);

		if (actualCabs.containsKey(cabCap)) {
			actualCabs.get(cabCap).put(cabRegn, actCab);
		} else {
			Map<String, ActualCabPreferences> actCabs = new HashMap<String, ActualCabPreferences>();
			actCabs.put(cabRegn, actCab);
			actualCabs.put(cabCap, actCabs);
		}

		return actCab;
	}

	private static boolean checkIfNewCabsAddedForThisRun() {
		for (Integer cabCap : actualCabs.keySet()) {
			Collection<ActualCabPreferences> actCabs = actualCabs.get(cabCap).values();
			for (ActualCabPreferences actCab : actCabs) {
				if (actCab.getCabRegnNum().startsWith("New-" + numIter + "-")) {
					return true;
				}
			}
		}
		return false;
	}

	private static void cleanAllocations() {
		prevIterAlloc = new HashMap<Route, ActualCabPreferences>();
		batch = 0;

		logger.debug("Cab;Route;Feasible;Score;Reason;Secondary Reason;"
				+ "Last Location Type;Last Location;Route Start;Route End;Next Location;Next Location Type;"
				+ "First Trip;Last Trip;" + "Empty Leg To Start;Empty Leg to Next;" + "Prev Trip End Time;This Trip End Time");

		for (Route route : routes.values()) {
			prevIterAlloc.put(route, route.getActualAllocatedCab());
			route.clearAllocation();
		}

		for (Integer cabCap : actualCabs.keySet()) {
			Collection<ActualCabPreferences> actCabs = actualCabs.get(cabCap).values();
			for (ActualCabPreferences actCab : actCabs) {

				// if(actCab.getCabRegnNum().startsWith("New-" + numIter + "-")) {
				// for(int i = 1; i<= actCab.getCurrentDuty(); i++) {
				// if(actCab.getDutyHours(i) >= 0.8 * 14) {
				// List<Route> cabRoutesInDuty = actCab.getAllocatedRoutes().get(i);
				// Collections.sort(cabRoutesInDuty, new SortRoutesByStartTime());
				//
				// Geocode routeStart = cabRoutesInDuty.get(0).getStartLoc();
				// Geocode routeEnd = cabRoutesInDuty.get(cabRoutesInDuty.size() - 1).getEndLoc();
				//
				// Geocode garage = getMidPoint(routeStart, routeEnd, 4);
				// actCab.setStartLoc(garage);
				// actCab.setEndLoc(garage);
				//
				// }
				// }
				// }

				if (actCab.getCurrentDuty() == 0) {
					continue;
				}

				Date startDutyTime = actCab.getFirstEventStart(1).getEventStart();
				Date cabPreferredStartTime = actCab.getStartTime();
				cabPreferredStartTime.setYear(startDutyTime.getYear());
				cabPreferredStartTime.setMonth(startDutyTime.getMonth());
				cabPreferredStartTime.setDate(startDutyTime.getDate());

				cabPreferredStartTime = new Date(
						cabPreferredStartTime.getTime() - ((actCab.getStartTimeThreshold() + 15) * minutes));

				if (startDutyTime.before(cabPreferredStartTime)) {
					cabPreferredStartTime = new Date(cabPreferredStartTime.getTime() - (24 * hours));
				}

				if (startDutyTime.getTime() - cabPreferredStartTime.getTime() > (3 * hours)) {
					actCab.setStartTime(cabPreferredStartTime);
					actCab.setEndTime(new Date(cabPreferredStartTime.getTime() + (14 * hours)));
				}

				actCab.clearAllEvents();
			}

		}

	}

}
