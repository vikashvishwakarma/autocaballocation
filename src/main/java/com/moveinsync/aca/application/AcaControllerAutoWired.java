package com.moveinsync.aca.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moveinsync.aca.services.FulfilmentResourceResponse;

@Service
public class AcaControllerAutoWired {
	@Autowired
	Allocator ac;
	
	@Autowired
	FulfilmentResourceResponse fulfilmentResourceResponse;

}
